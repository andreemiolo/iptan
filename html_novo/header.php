<div id="barTop">
    <div class="serviceOnline">
        <div class="centerContent">
            <ul>
                <li>Aluno</li>
                <li><a href="http://186.194.210.79/corpore.net/Login.aspx" target="_blank">Portal Educacional</a></li>
                <!--<li><a href="#">Avaliação Institucional</a></li>-->
                <li><a href="http://186.194.210.79/corpore.net/Source/Bib-Biblioteca/Public/BibFirewall.htm?CodColigada=1&CodFilial=1&CodUnidade=1" target="_blank">Biblioteca</a></li>
                <!--<li><a href="#">Pesquisa</a></li>
                <li><a href="#">Extenção</a></li>-->
                <li><a href="http://www.iptan.soshost.com.br/solicitacao_ouvidoria.php" target="_blank">Ouvidoria</a></li>
                <li><a href="http://www.iptan.edu.br/arquivos-gerais/calendario-academico.pdf" target="_blank">Calendário Acadêmico</a></li>
            </ul>
            <ul>
                <li>Professores e Funcionários</li>
                <li><a href="http://186.194.210.79/corpore.net/Login.aspx" target="_blank">Portal Educacional</a></li>
                <li><a href="https://webmail-seguro.com.br/iptan.edu.br/" target="_blank">Webmail</a></li>
                <li><a href="http://www.iptan.edu.br/sisti/index.php" target="_blank">SISTI</a></li>
                <li><a href="http://www.iptan.edu.br/reserva_equip/index.php" target="_blank">SIS Reserva</a></li>
                <li><a href="http://www.iptan.soshost.com.br/solicitacao_ouvidoria.php" target="_blank">Ouvidoria</a></li>
                <li><a href="http://186.194.210.79/corpore.net/Source/Bib-Biblioteca/Public/BibFirewall.htm?CodColigada=1&CodFilial=1&CodUnidade=1" target="_blank">Biblioteca</a></li>
            </ul>
            <!--<ul>
                <li>Professores e Funcionários</li>
                <li><a href="#">Webmail</a></li>
                <li><a href="#">SISTI</a></li>
                <li><a href="#">SIS Reserva</a></li>
                <li><a href="#">Ouvidoria</a></li>
                <li><a href="#">Biblioteca</a></li>
            </ul>-->
        </div>
    </div>
    <div id="barCenter">
         <div id="btnServices">
            <i class="fa fa-cog" aria-hidden="true"></i>
            <span>SERVIÇOS ONLINE</span>
        </div>
        <div id="btnMenu">
            <i class="fa fa-bars" aria-hidden="true"></i>
            <i class="fa fa-times" aria-hidden="true"></i>
        </div>
        <a href="index.php" id="logoIptan">
            <img src="images/logo-iptan.svg" id="logoPluss">
            <img src="images/assinatura-top.svg" id="logoReduce">
        </a>
        
         <div id="search">
            <div class="form-group">
            <input class="form-control" placeholder="Pesquisar" type="text">
                <span class="glyphicon glyphicon-search form-control-feedback" aria-hidden="true"></span>
                </div>
        </div>
        
    </div>
    <ul id="menuHome">
            <li>
                <a href="#">Institucional</a>
                <ul class="submenu">
                    <li>
                        <a href="iptan.php">O IPTAN</a>
                    </li>
                    <li>
                        Administrativo
                        <ul>
                            <li>
                                <a href="biblioteca.php">Biblioteca</a>
                            </li>
                            <li>
                                <a href="extensaouniversitaria.php">Coordenação de Extensão</a>
                            </li>
                            <li>
                                <a href="coordenacaodepesquisa.php">Coordenação de Pesquisa</a>
                            </li>
                            <li>
                                <a href="financeiro.php">Financeiro</a>
                            </li>
                            <li>
                                <a href="nucleodeapoioaoestudante.php">Núcleo de Apoio ao Estudante</a>
                            </li>
                            <li>
                                <a href="secretariaacademica.php">Secretária Acadêmica</a>
                            </li>
                            <li>
                                <a href="tecnologiadainformacao.php">Tecnologia da Informação </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="laboratorios.php">Laboratórios</a>
                    </li>
                    <li>
                        <a href="avaliacaoinstitucional.php">Avaliação Institucional</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#">Graduação</a>
                <ul class="submenu">
                    <li>
                        <a href="http://www.iptan.edu.br/novovestibular/" target="_blank">Vestibular</a>
                    </li>
                    <li>
                        <a href="cursos.php">Cursos</a>
                    </li>
                    <!--<li>
                        <a href="#">Matrícula e Rematrícula</a>
                    </li>-->
                    <li>
                        <a href="http://www.iptan.edu.br/arquivos-gerais/calendario-academico.pdf" target="_blank">Calendário Acadêmico</a>
                    </li>
                    <li>
                        <a href="http://www.iptan.edu.br/creditar/" target="_blank">Financiamento Privado</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="http://www.itpac.br/sites/educacao-a-distancia" target="_blank">EAD</a>
                <!--<ul class="submenu">
                    <li>
                        <a href="#">EAD</a>
                    </li>
                    <li>
                        <a href="#">Polo EAD IPTAN</a>
                    </li>
                </ul>-->
            </li>
            <li>
                <a href="pesquisaiptan.php">Pesquisa</a>
                <!--<ul class="submenu">
                    <li>
                        <a href="#">Pesquisa em Andamento</a>
                    </li>
                    <li>
                        <a href="#">Iniciação Científica</a>
                    </li>
                    <li>
                        <a href="#">Publicações</a>
                    </li>
                </ul>-->
            </li>
            <li>
                <a href="extensaoiptan.php">Extensão</a>
                <!--<ul class="submenu">
                    <li>
                        <a href="#">Cursos</a>
                    </li>
                    <li>
                        <a href="#">Projetos IPTAN</a>
                    </li>
                </ul>-->
            </li>
            <li>
                <a href="http://www.iptan.edu.br/novovestibular/" target="_blank">Vestibular</a>
                <!--<ul class="submenu">
                    <li>
                        <a href="#">Vestibular de Medicina</a>
                    </li>
                    <li>
                        <a href="#">Vestibular Geral</a>
                    </li>
                </ul>-->
            </li>
            <li>
                <a href="noticias.php">Notícias</a>
            </li>
            <li>
                <a href="#">Contato</a>
                <ul class="submenu">
                    <li>
                        <a href="contato.php">Fale com IPTAN</a>
                    </li>
                    <!--<li>
                        <a href="#">Ramais</a>
                    </li>-->
                </ul>
            </li>
        </ul>
</div>