<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>IPTAN</title>
	<meta name="Author" content=""/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon-16x16.png">
    
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/html-reset.css">
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
    <link rel="stylesheet" type="text/css" href="css/coringa.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/base.css">
    <link rel="stylesheet" type="text/css" href="css/vendor.min.css">
    <link href='https://fonts.googleapis.com/css?family=Pontano+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <script src="js/modernizr.custom.js"></script>
</head>
<body>
    
<!-- <a href="#" class="btnCourse">
    Quero saber mais sobre este curso
</a>
-->

<? require 'header.php'; ?>      
    
    
<div id="content">
    <ul id="breadcrumb">
        <li>
            <a href="index.php">Home</a>
        </li>
        <li>
            <a href="#">Graduação</a>
        </li>
        <li>
            <a href="cursos.php">Cursos</a>
        </li>
        <li>
            <a href="engenhariadeproducao.php">Engenharia de Produção</a>
        </li>
    </ul>
    
    <div class="standardTitle egp">
        <div class="centerContent">
            <div id="redes">
                <div class="fb-share-button" 
                    data-href="http://www.your-domain.com/your-page.php" 
                    data-layout="button_count">
                </div>
                <div class="g-plus" data-action="share" data-annotation="bubble"></div>
            </div>
            <h1 class="titleDefault">
                <span>Graduação</span>
                <p>Engenharia de Produção</p><br />
            </h1>
            <div class="text">
                <p align="justify">O projeto pedagógico do curso de Engenharia de Produção do IPTAN tem sua conceituação nas diretrizes da ABEPRO (Associação Brasileira de Engenharia de Produção) e nas orientações do CNE/CES 11/2002. A ABEPRO é uma entidade que congrega estudantes, profissionais, professores e cursos de graduação e pós-graduação relacionados à Engenharia de Produção de todo o país.<br />
                Compete à Engenharia de Produção o projeto, a modelagem, a implantação, a operação, a manutenção e a melhoria de sistemas produtivos integrados de bens e serviços, envolvendo homens, recursos financeiros e materiais, tecnologia, informação e energia. Compete ainda especificar, prever e avaliar os resultados obtidos destes sistemas para a sociedade e o meio ambiente, recorrendo a conhecimentos especializados da matemática, física, ciências humanas e sociais, conjuntamente com os princípios e métodos de análise e projeto da engenharia.<br />
                A Engenharia de Produção, ao voltar a sua ênfase para características de produtos (bens e/ou serviços) e de sistemas produtivos, vincula-se fortemente com as ideias de projetar e viabilizar produtos e sistemas produtivos, planejar a produção, produzir e distribuir produtos que a sociedade valoriza.
                </p>
            </div>
            
            <ul class="topicsTitle egpcolor">
                <li>
                    <span class="centericon fa fa-graduation-cap fa-2x"></span><h5 class="textbox">Graduação</h5>
                    <p class="textbox">Bacharelado</p>
                </li>
                <li>
                    <span class="centericon fa fa-dollar fa-2x"></span><h5 class="textbox">Valor do Curso</h5>
                    <p class="textbox">R$ 927,00</p>
                </li>
                <li>
                    <span class="centericon fa fa-flag fa-2x"></span><h5 class="textbox">Área de Conhecimento</h5>
                    <p class="textbox">Engenharias e Computação</p>
                </li>
                <li>
                    <span class="centericon fa fa-calendar-o fa-2x"></span><h5 class="textbox">Duração do Curso</h5>
                    <p class="textbox">10 semestres</p>
                </li>
            </ul>
        </div>
    </div>
    
    <div class="centerContent">
        
        <div class="barLeft">
            <h1 class="titleDefault2">
                <span>Coordenação</span>
                <p style="font-size:20px;">MARCOS SÁVIO DE SOUZA</p>
            </h1>
            <p align="justify">
				Possui graduação em Engenharia Mecânica Operacional pela Universidade Federal de São João Del-Rei - UFSJ(1979), graduação em Engenharia Industrial Mecânica pela Universidade Federal de São João Del-Rei - UFSJ (1981) e mestrado em Engenharia Mecânica pela Universidade Estadual de Campinas UNICAMP (1989). Exerceu vários cargos de direção na UFSJ, onde encerrou sua carreira como coordenador do curso de engenharia de produção, cargo que exerceu desde sua implantação até a avaliação do Ministério da Educação com conceito 4. Atualmente é coordenador e professor no (IPTAN). Atuou como orientador do Projeto BAJA/SAE durante seis anos e como tutor de empresa júnior Mecânica/Produção pelo mesmo período. Preside a Associação Brasil Alemanha de Desenvolvimento Social - ABADS, desde 2005 onde são desenvolvidos vários projetos sociais com a participação de vinte e cinco bolsistas de vários cursos de graduação da UFSJ. 
            </p>
            <!--<div class="tagsSearch">
                <h3>Tags</h3>
                <span>CURSO</span> <span>IPTAN</span> <span>PALESTRA</span> <span>EXTENSÃO</span> <span>VESTIBULAR</span>
            </div>-->
        </div>
        <div class="barRight">
            <h5>
                <i class="fa fa-link" aria-hidden="true"></i>
                <span>Matriz Curricular</span>
            </h5>
            <ul>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/engprod/matriz-curricular.pdf" target="_blank">Matriz</a>
                </li>
            </ul>
            <h5>
                <i class="fa fa-arrow-down" aria-hidden="true"></i>
                <span>Arquivos</span>
            </h5>
            <ul>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/horarios_de_aula/horario_aula_engprod.pdf" target="_blank">Horário de aula</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/engprod/ppc.pdf" target="_blank">Projeto Pedagógico</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/engprod/horario-coordenacao.pdf" target="_blank">Horário de Coordenação</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/engprod/carga-horaria-total.pdf" target="_blank">Carga Horária Total do Curso</a>
                </li>
            </ul>
            <h5>
                <i class="fa fa-link" aria-hidden="true"></i>
                <span>Plano de Ensino</span>
            </h5>
            <ul>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/engprod/plano-de-ensino/2-periodo.rar">2<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
            </ul>
        </div>
        
    </div>        
    
</div>
    
    
<? require 'footer.php'; ?> 
    
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.hoverdir.js" type="text/javascript"></script>
    
    
    <script>
        
        $("#btnMenu").click(function(){
        $("#btnMenu").toggleClass("active");
    });
        
    $("#openCloseMap").click(function(){
        $("#mapSite").toggleClass("openMapsFooter");
        
        var openMap = $("#mapSite").hasClass("openMapsFooter");
        if(openMap == true){
            $("#openCloseMap .fa-times").css("display", "inline-block");
            $("#openCloseMap .fa-expand").css("display", "none");
        }else{
            $("#openCloseMap .fa-times").css("display", "none");
            $("#openCloseMap .fa-expand").css("display", "inline-block");
        }
    });
        
        $(function() {
			
				$(' .listDetails > li ').each( function() { $(this).hoverdir(); } );

			});
    $("#menuHome > li").hover(function(){
        if ($(this).hasClass('active')) {
            
        } else {
            $("#menuHome > li").removeClass("active");
        }
    });
     $("#btnServices").click(function(){
        $(".serviceOnline").toggleClass("active");
    });
    $("#menuHome > li").click(function(){
        $("#menuHome > li").removeClass("active");
        $(this).addClass("active");
    });
        
        $("#btnMenu").click(function(){
        $("#menuHome").toggleClass("active");
        $("body").toggleClass("bodyFix");
    });
        
    
    var widthSite = $(window).width();
    if(widthSite >= 1050){
        $(window).scroll(function (event) {
                var rolado = $(window).scrollTop();
                if (rolado > 100) {
                    $("#barTop").addClass("reduce");
                } else {
                    $("#barTop").removeClass("reduce");
                }
            });
        }
    </script>

    
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.6";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    
    
<script src="https://apis.google.com/js/platform.js" async defer>
        {lang: 'pt-BR'}
    </script>
   <script src="js/jquery.waypoints.min.js"></script>
   <script src="js/jquery.magnific-popup.min.js"></script>  
<script>
(function($) {
    $('.item-wrap a').magnificPopup({
       type:'inline',
       fixedContentPos: false,
       removalDelay: 300,
       showCloseBtn: false,
       mainClass: 'mfp-fade'

    });
    $(document).on('click', '.popup-modal-dismiss', function (e) {
    		e.preventDefault();
    		$.magnificPopup.close();
    });
})(jQuery);
</script>

</body>
</html>
