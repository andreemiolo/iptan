<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>IPTAN</title>
	<meta name="Author" content=""/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon-16x16.png">
    
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/html-reset.css">
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
    <link rel="stylesheet" type="text/css" href="css/coringa.css">
    <link href='https://fonts.googleapis.com/css?family=Pontano+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <script src="js/modernizr.custom.js"></script>
</head>
<body>

<? require 'header.php'; ?>      
    
    
<div id="content">
    <ul id="breadcrumb">
        <li>
            <a href="index.php">Home</a>
        </li>
        <li>
            <a href="pesquisaiptan.php">Pesquisa</a>
        </li>
    </ul>
    
    <div class="standardTitle">
        <div class="centerContent">
            <div id="redes">
                <div class="fb-share-button" 
                    data-href="http://www.your-domain.com/your-page.php" 
                    data-layout="button_count">
                </div>
                <div class="g-plus" data-action="share" data-annotation="bubble"></div>
            </div>
            <div class="titleDefaultWip">
                <h2><div class="fa fa-cogs"></div></h2> 
                <h4>Página em construção!</h4>
                <h3>
                    Em breve esta área estará disponível! ;)
                </h3>
            </div>

        </div>
    </div>
    
    
</div>
    
    
<? require 'footer.php'; ?> 
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.hoverdir.js" type="text/javascript"></script>
    
    
    <script>
        
        $("#btnMenu").click(function(){
        $("#btnMenu").toggleClass("active");
    });
        
    $("#openCloseMap").click(function(){
        $("#mapSite").toggleClass("openMapsFooter");
        
        var openMap = $("#mapSite").hasClass("openMapsFooter");
        if(openMap == true){
            $("#openCloseMap .fa-times").css("display", "inline-block");
            $("#openCloseMap .fa-expand").css("display", "none");
        }else{
            $("#openCloseMap .fa-times").css("display", "none");
            $("#openCloseMap .fa-expand").css("display", "inline-block");
        }
    });
        
        $(function() {
			
				$(' .listDetails > li ').each( function() { $(this).hoverdir(); } );

			});
    $("#menuHome > li").hover(function(){
        if ($(this).hasClass('active')) {
            
        } else {
            $("#menuHome > li").removeClass("active");
        }
    });
     $("#btnServices").click(function(){
        $(".serviceOnline").toggleClass("active");
    });
    $("#menuHome > li").click(function(){
        $("#menuHome > li").removeClass("active");
        $(this).addClass("active");
    });
        
        $("#btnMenu").click(function(){
        $("#menuHome").toggleClass("active");
        $("body").toggleClass("bodyFix");
    });
        
    
    var widthSite = $(window).width();
    if(widthSite >= 1050){
        $(window).scroll(function (event) {
                var rolado = $(window).scrollTop();
                if (rolado > 100) {
                    $("#barTop").addClass("reduce");
                } else {
                    $("#barTop").removeClass("reduce");
                }
            });
        }
    </script>

    
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.6";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    
    
<script src="https://apis.google.com/js/platform.js" async defer>
        {lang: 'pt-BR'}
    </script>

</body>
</html>
