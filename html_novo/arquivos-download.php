<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>IPTAN</title>
	<meta name="Author" content=""/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon-16x16.png">
    
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/html-reset.css">
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
    <link rel="stylesheet" type="text/css" href="css/resultado-pesquisa.css">
    <link href='https://fonts.googleapis.com/css?family=Pontano+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <script src="js/modernizr.custom.js"></script>
</head>
<body>
    

<? require 'header.php'; ?>      
    
    
<div id="content">
   

<!-- NOTÍCIAS -->
    <div id="localNews">
        <div class="centerContent">
            <h1>Arquivos</h1>
            <div id="categoryFiles">
                <select class="form-control">
                    <option>Exibir por</option>
                    <option>Institucional</option>
                    <option>Extensão</option> 
                    <option>Pesquisa</option> 
                    <option>Vestibular</option> 
                    <option>Graduação</option> 
                    <option>EAD</option> 
                </select>
            </div>
            
            <ul class="resulteSearch">
                <li>
                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                    <span>08/02/2016</span>
                    <a href="#">
                        <figure>
                            <img src="images/news.jpg">
                        </figure>
                        <div class="titleText">
                            <h3>Lorem ipsum dolor sit amet, consectetur</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna a...</p>
                        </div>
                    </a>
                </li>
                <li>
                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                    <span>08/02/2016</span>
                    <a href="#">
                        <figure>
                            <i class="fa fa-link" aria-hidden="true"></i>
                        </figure>
                        <div class="titleText">
                            <h3>Lorem ipsum dolor sit amet, consectetur</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna a...</p>
                        </div>
                    </a>
                </li>
                <li>
                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                    <span>08/02/2016</span>
                    <a href="#">
                        <figure>
                            <img src="images/news.jpg">
                        </figure>
                        <div class="titleText">
                            <h3>Lorem ipsum dolor sit amet, consectetur</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna a...</p>
                        </div>
                    </a>
                </li>
                <li>
                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                    <span>08/02/2016</span>
                    <a href="#">
                        <figure>
                            <img src="images/news.jpg">
                        </figure>
                        <div class="titleText">
                            <h3>Lorem ipsum dolor sit amet, consectetur</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna a...</p>
                        </div>
                    </a>
                </li>
            </ul>
            
            
            <ul id="page">
                <li>
                    <a href="#">...</a>
                </li>
                <li>
                    <a href="#">1</a>
                </li>
                <li>
                    <a href="#">2</a>
                </li>
                <li>
                    <a href="#">3</a>
                </li>
                <li>
                    <a href="#">4</a>
                </li>
                <li>
                    <a href="#">...</a>
                </li>
            </ul>
            
        </div>
    </div>
<!-- NOTÍCIAS -->
    
    
    
    
</div>
    

<? require 'footer.php'; ?> 
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.hoverdir.js" type="text/javascript"></script>
    
    <script>
        $("#btnMenu").click(function(){
        $("#btnMenu").toggleClass("active");
    });
        
    $("#listDates li .clickCalendar").click(function(){
        $("#listDates li .clickCalendar").removeClass("active");
        $(this).addClass("active");
        $(".tootipDescription").removeClass("active");
        $(this).next(".tootipDescription").addClass("active");
    });
    $(".tootipDescription").click(function(){
        $(".clickCalendar").removeClass("active");
        $(".tootipDescription").removeClass("active");
    });
    $(".clickCalendar, .tootipDescription").click(function(){
        var viewActive = $(".clickCalendar").hasClass("active");
        if(viewActive == true){
            $(".clickCalendar").css("opacity", "0.5");
        }else{
            $(".clickCalendar").css("opacity", "1");
        }
    });
        
    $("#openCloseMap").click(function(){
        $("#mapSite").toggleClass("openMapsFooter");
        
        var openMap = $("#mapSite").hasClass("openMapsFooter");
        if(openMap == true){
            $("#openCloseMap .fa-times").css("display", "inline-block");
            $("#openCloseMap .fa-expand").css("display", "none");
        }else{
            $("#openCloseMap .fa-times").css("display", "none");
            $("#openCloseMap .fa-expand").css("display", "inline-block");
        }
    });
        
        $(function() {
			
				$(' .listDetails > li ').each( function() { $(this).hoverdir(); } );

			});
    $("#menuHome > li").hover(function(){
        if ($(this).hasClass('active')) {
            
        } else {
            $("#menuHome > li").removeClass("active");
        }
    });
     $("#btnServices").click(function(){
        $(".serviceOnline").toggleClass("active");
    });
    $("#menuHome > li").click(function(){
        $("#menuHome > li").removeClass("active");
        $(this).addClass("active");
    });
        
        $("#btnMenu").click(function(){
        $("#menuHome").toggleClass("active");
        $("body").toggleClass("bodyFix");
    });
        
    
    var widthSite = $(window).width();
    if(widthSite >= 1050){
        $(window).scroll(function (event) {
                var rolado = $(window).scrollTop();
                if (rolado > 100) {
                    $("#barTop").addClass("reduce");
                } else {
                    $("#barTop").removeClass("reduce");
                }
            });
        }
        
        
    //SLIDE
    $('.carousel').carousel();
    </script>

    
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.6";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

</body>
</html>
