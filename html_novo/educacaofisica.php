<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>IPTAN</title>
	<meta name="Author" content=""/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon-16x16.png">
    
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/html-reset.css">
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
    <link rel="stylesheet" type="text/css" href="css/coringa.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/base.css">
    <link rel="stylesheet" type="text/css" href="css/vendor.min.css">
    <link href='https://fonts.googleapis.com/css?family=Pontano+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <script src="js/modernizr.custom.js"></script>
</head>
<body>
    
<!-- <a href="#" class="btnCourse">
    Quero saber mais sobre este curso
</a>
-->

<? require 'header.php'; ?>      
    
    
<div id="content">
    <ul id="breadcrumb">
        <li>
            <a href="index.php">Home</a>
        </li>
        <li>
            <a href="#">Graduação</a>
        </li>
        <li>
            <a href="cursos.php">Cursos</a>
        </li>
        <li>
            <a href="educacaofisica.php">Educação Física</a>
        </li> 
    </ul>
    
    <div class="standardTitle edf">
        <div class="centerContent">
            <div id="redes">
                <div class="fb-share-button" 
                    data-href="http://www.your-domain.com/your-page.php" 
                    data-layout="button_count">
                </div>
                <div class="g-plus" data-action="share" data-annotation="bubble"></div>
            </div>
            <h1 class="titleDefault">
                <span>Graduação</span>
                <p>Educação Física</p><br />
            </h1>
            <div class="text">
                <p align="justify">O profissional de Educação Física planeja, ensina, orienta, coordena e avalia a prática de atividades físicas, esportivas e recreativas nas perspectivas da promoção e proteção da saúde, da qualidade de vida, da educação e da reeducação motora, do rendimento físico-esportivo, do lazer e da gestão de empreendimentos relacionados a essa atividade profissional.<br />
                O objetivo do Curso é: Qualificar e formar o profissional de Educação Física para intervir na sociedade, no campo das atividades corporais, considerando os diferentes sujeitos, os objetivos e os meios, demonstrando competência técnica, atitude científica e comprometimento ético.<br />
                A modalidade <b>Bacharelado</b> forma profissionais de Educação Física para atuação com as atividades físicas e esportivas, por meio das diferentes manifestações e expressões do movimento humano (Resolução de 07 de maio de 2004 – art. 4º par. 1º).<br />
                A formação em <b>Licenciatura</b> em Educação Física do IPTAN supõe um perfil científico e humanista, com competências técnico-pedagógicas e socioculturais, sendo capaz de socializar crianças, jovens e adultos com a cultura do movimento humano, conscientizá-los e motivá-los para a adoção de um estilo de vida fisicamente ativo e saudável, balizado por uma postura ética, crítica, reflexiva, participativa e solidária.

                </p>
            </div>
            
            <ul class="topicsTitle edfcolor">
                <li>
                    <span class="centericon fa fa-graduation-cap fa-2x"></span><h5 class="textbox">Graduação</h5>
                    <p class="textbox">Bacharelado e Licenciatura</p>
                </li>
                <li>
                    <span class="centericon fa fa-dollar fa-2x"></span><h5 class="textbox">Valor do Curso</h5>
                    <p class="textbox">R$ 558,00</p>
                </li>
                <li>
                    <span class="centericon fa fa-flag fa-2x"></span><h5 class="textbox">Área de Conhecimento</h5>
                    <p class="textbox">Ciências Médicas e da Saúde</p>
                </li>
                <li>
                    <span class="centericon fa fa-calendar-o fa-2x"></span><h5 class="textbox">Duração do Curso</h5>
                    <p class="textbox">8 semestres</p>
                </li>
            </ul>
        </div>
    </div>
    
    <div class="centerContent">
        
        <div class="barLeft">
            <h1 class="titleDefault2">
                <span>Coordenação</span>
                <p style="font-size:20px;">Rubens Bagni Torres</p>
            </h1>
            <p align="justify">
                Graduado em Educação Física pela UFMG (Universidade Federal de Minas Gerais, 1991-1995).Mestre pela Universidade Federal de São João Del Rei no PPMEC (Programa de Pós Graduação em Engenharia Mecânica - Materias e Processos de Fabricação). Atualmente é Coordenador e Professor do curso de Educação Física do Instituto de Ensino superior Presidente Tancredo de Almeida Neves. Professor do NEAD da UFSJ e Proprietário e Personal Trainer da Apollo Academia situada em São João del Rei/MG. Trabalhou como coordenador, treinador e professor de Educação Física do Colégio Instituto Auxiliadora durante 15 anos, atuando principalmente nos temas: Desportos com bola e Atividades de Aventura. Ministra cursos e palestras sobre o Esporte Orientação alem de ser consultor e organizador de eventos esportivos principalmente os relacionados a natureza. Árbitro pela Confederação Brasileira de Orientação e Coordenador técnico do Cosele (Clube de Orientação Serra do Lenheiro).
            </p>
            <!--<div class="tagsSearch">
                <h3>Tags</h3>
                <span>CURSO</span> <span>IPTAN</span> <span>PALESTRA</span> <span>EXTENSÃO</span> <span>VESTIBULAR</span>
            </div>-->
        </div>
        <div class="barRight">
            <div class="blockRight">
            <h5>
                <i class="fa fa-link" aria-hidden="true"></i>
                <span>Matriz Curricular</span>
            </h5>
            <ul>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/contabeis/matriz-curricular.pdf" target="_blank">Matriz</a>
                </li>
            </ul>
            </div>
            <div class="blockRight">
            <h5>
                <i class="fa fa-arrow-down" aria-hidden="true"></i>
                <span>Arquivos</span>
            </h5>
            <ul>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/horarios_de_aula/horario_aula_edfisica.pdf" target="_blank">Horário de aula</a>
                </li>
            </ul>
            </div>
            <div class="blockRight">
            <h5>
                <i class="fa fa-link" aria-hidden="true"></i>
                <span>Plano de Ensino</span>
            </h5>
            <ul><h4>Bacharelado</h4>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/edfisica/plano-de-ensino-bacharelado/1-periodo.rar" target="_blank">1<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/edfisica/plano-de-ensino-bacharelado/2-periodo.rar" target="_blank">2<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <h4>Licenciatura</h4>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/edfisica/plano-de-ensino-licenciatura/1-periodo.rar" target="_blank">1<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/edfisica/plano-de-ensino-licenciatura/2-periodo.rar" target="_blank">2<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/edfisica/plano-de-ensino-licenciatura/3-periodo.rar" target="_blank">3<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/edfisica/plano-de-ensino-licenciatura/4-periodo.rar" target="_blank">4<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/edfisica/plano-de-ensino-licenciatura/5-periodo.rar" target="_blank">5<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/edfisica/plano-de-ensino-licenciatura/6-periodo.rar" target="_blank">6<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/edfisica/plano-de-ensino-licenciatura/8-periodo.rar" target="_blank">7<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/contabeis/plano-de-ensino-licenciatura/8-periodo.rar" target="_blank">8<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
            </ul>
            </div>
        </div>
        
    </div>
       
    
</div>
    
    
<? require 'footer.php'; ?> 
    
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.hoverdir.js" type="text/javascript"></script>
    
    
    <script>
        
        $("#btnMenu").click(function(){
        $("#btnMenu").toggleClass("active");
    });
        
    $("#openCloseMap").click(function(){
        $("#mapSite").toggleClass("openMapsFooter");
        
        var openMap = $("#mapSite").hasClass("openMapsFooter");
        if(openMap == true){
            $("#openCloseMap .fa-times").css("display", "inline-block");
            $("#openCloseMap .fa-expand").css("display", "none");
        }else{
            $("#openCloseMap .fa-times").css("display", "none");
            $("#openCloseMap .fa-expand").css("display", "inline-block");
        }
    });
        
        $(function() {
			
				$(' .listDetails > li ').each( function() { $(this).hoverdir(); } );

			});
    $("#menuHome > li").hover(function(){
        if ($(this).hasClass('active')) {
            
        } else {
            $("#menuHome > li").removeClass("active");
        }
    });
     $("#btnServices").click(function(){
        $(".serviceOnline").toggleClass("active");
    });
    $("#menuHome > li").click(function(){
        $("#menuHome > li").removeClass("active");
        $(this).addClass("active");
    });
        
        $("#btnMenu").click(function(){
        $("#menuHome").toggleClass("active");
        $("body").toggleClass("bodyFix");
    });
        
    
    var widthSite = $(window).width();
    if(widthSite >= 1050){
        $(window).scroll(function (event) {
                var rolado = $(window).scrollTop();
                if (rolado > 100) {
                    $("#barTop").addClass("reduce");
                } else {
                    $("#barTop").removeClass("reduce");
                }
            });
        }
    </script>

    
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.6";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    
    
<script src="https://apis.google.com/js/platform.js" async defer>
        {lang: 'pt-BR'}
    </script>
   <script src="js/jquery.waypoints.min.js"></script>
   <script src="js/jquery.magnific-popup.min.js"></script>  
<script>
(function($) {
    $('.item-wrap a').magnificPopup({
       type:'inline',
       fixedContentPos: false,
       removalDelay: 300,
       showCloseBtn: false,
       mainClass: 'mfp-fade'

    });
    $(document).on('click', '.popup-modal-dismiss', function (e) {
    		e.preventDefault();
    		$.magnificPopup.close();
    });
})(jQuery);
</script>

</body>
</html>
