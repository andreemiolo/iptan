<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>IPTAN</title>
	<meta name="Author" content=""/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon-16x16.png">
    
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/html-reset.css">
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
    <link rel="stylesheet" type="text/css" href="css/coringa.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/base.css">
    <link rel="stylesheet" type="text/css" href="css/vendor.min.css">
    <link href='https://fonts.googleapis.com/css?family=Pontano+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <script src="js/modernizr.custom.js"></script>
</head>
<body>
    
<!-- <a href="#" class="btnCourse">
    Quero saber mais sobre este curso
</a>
-->

<? require 'header.php'; ?>    

    
<div id="content">
    <ul id="breadcrumb">
        <li>
            <a href="index.php">Home</a>
        </li>
        <li>
            <a href="#">Graduação</a>
        </li>
        <li>
            <a href="cursos.php">Cursos</a>
        </li>
        <li>
            <a href="administracao.php">Administração</a>
        </li>        
    </ul>
    
    <div class="standardTitle adm">
        <div class="centerContent">
            <div class="btnShare">
                <i class="fa fa-share-alt" aria-hidden="true"></i>
            </div>
            <div id="redes">
                <div class="fb-share-button" 
                    data-href="http://www.your-domain.com/your-page.php" 
                    data-layout="button_count">
                </div>
                <div class="g-plus" data-action="share" data-annotation="bubble"></div>
            </div>
            <h1 class="titleDefault">
                <span>Graduação</span>
                <p>Administração</p><br />
            </h1>
            <div class="text">
                <p align="justify">O profissional de administração é aquele que controla os recursos de uma empresa, sejam eles humanos, financeiros ou materiais. Além de atuar em empresas de qualquer área, dentro da companhia ele tem a capacidade para trabalhar nos mais diversos setores, indo das finanças ao marketing, passando por recursos humanos, logística e produção. Sua atuação inclui, ainda, controle de custos, planejamento estratégico, gerenciamento de vendas e gestão da inovação. Seja qual for sua área de atuação, o Administrador deve estar sempre atualizado em relação às novas tendências do mercado que possam influenciar os resultados da empresa em que atua.<br />
                Destaque na cidade e região por sua excelência na qualidade do ensino, o curso de Administração do IPTAN proporciona uma formação atual, completa e alinhada às principais demandas do mercado. Sua matriz curricular foi elaborada com o objetivo de desenvolver a capacidade empreendedora do aluno, estimulando sua atuação consciente e voltada para o desenvolvimento sustentável, sempre em prol da melhoria da qualidade de vida das pessoas.
                </p>
            </div>
            
            <ul class="topicsTitle admcolor">
                <li>
                    <span class="centericon fa fa-graduation-cap fa-2x"></span><h5 class="textbox">Graduação</h5>
                    <p class="textbox">Bacharelado</p>
                </li>
                <li>
                    <span class="centericon fa fa-dollar fa-2x"></span><h5 class="textbox">Valor do Curso</h5>
                    <p class="textbox">R$ 498,00</p>
                </li>
                <li>
                    <span class="centericon fa fa-flag fa-2x"></span><h5 class="textbox">Área de Conhecimento</h5>
                    <p class="textbox">Ciências Sociais Aplicadas</p>
                </li>
                <li>
                    <span class="centericon fa fa-calendar-o fa-2x"></span><h5 class="textbox">Duração do Curso</h5>
                    <p class="textbox">8 semestres</p>
                </li>
            </ul>
        </div>
    </div>
    
    <div class="centerContent">
        
        <div class="barLeft">
            <h1 class="titleDefault2">
                <span>Coordenação</span>
                <p style="font-size:20px;">Leonardo Henrique de Almeida e Silva</p>
            </h1>
            <p align="justify">
                Professor e coordenador do curso de Administração do Instituto de Ensino Superior "Presidente Tancredo de Almeida Neves", atuando ainda como professor de Economia nos cursos de Direito, Ciências Contábeis e Engenharia de Produção. Professor de Ensino Básico, Técnico e Tecnológico do Instituto Federal de Educação, Ciência e Tecnologia/ Câmpus São João del-Rei, na área de Gestão. Atua como orientador de TCC's dos cursos de graduação em Administração Pública, licenciatura em Pedagogia e especialização em Gestão Pública do Núcleo de Educação à Distância da Universidade Federal de São João del-Rei. Já lecionou em Pós-Graduação nas áreas de "Cooperativismo e Agronegócio" e "Economia e Mercado". Foi professor substituto do Departamento de Ciências Econômicas da UFSJ nos anos de 2005 e 2006, professor e coordenador do curso de Administração da Faculdade Presidente Antônio Carlos de São João del-Rei de 2006 a 2012 e funcionário concursado da Caixa Econômica Federal de 2006 a 2013. É Mestre em Economia pela Universidade Federal do Espírito Santo (2005), e possui graduação em Administração pela Faculdade Machado Sobrinho (2002) e em Ciências Econômicas pela Universidade Federal de Juiz de Fora (2003). Foi homenageado com o prêmio "Profissionais do Ano", categoria Professor, pelo jornal Tribuna Sanjoanense nos anos de 2010 e 2011, além de várias homenagens de formandos nos cursos em que já lecionou. É o organizador da Semana da Administração do IPTAN, principal evento na área na cidade. (Texto informado pelo autor)
            </p>
            <!--<div class="tagsSearch">
                <h3>Tags</h3>
                <span>CURSO</span> <span>IPTAN</span> <span>PALESTRA</span> <span>EXTENSÃO</span> <span>VESTIBULAR</span>
            </div>-->
        </div>
        
        <div class="btnFiles">
            <button>
                <p>
                    <i class="fa fa-arrow-down" aria-hidden="true"></i>
                    <span>Download</span>
                </p>
                <p>Faça o download dos arquivos desse curso.</p>
            </button>
        </div>
        
        <div class="barRight">
            <div class="blockRight">
            <h5>
                <i class="fa fa-link" aria-hidden="true"></i>
                <span>Matriz Curricular</span>
            </h5>
            <ul>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/adm/matriz-curricular-1.pdf" target="_blank">Matriz 1</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/adm/matriz-curricular-2.pdf" target="_blank">Matriz 2</a>
                </li>
            </ul>
            </div>
            
            <div class="blockRight">
            <h5>
                <i class="fa fa-arrow-down" aria-hidden="true"></i>
                <span>Arquivos</span>
            </h5>
            <ul>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/horarios_de_aula/horario_aula_adm.pdf" target="_blank">Horário de aula</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/adm/ppc.pdf" target="_blank">Projeto Pedagógico</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/adm/anexos-ppc.pdf" target="_blank">Projeto Pedagógico - Anexos</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/adm/regulamentos.pdf" target="_blank">Regulamentos</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/adm/calendario_adm-2014.pdf" target="_blank">Calendário do Curso</a>
                </li>
            </ul>
            </div>
            
            <div class="blockRight">
            <h5>
                <i class="fa fa-link" aria-hidden="true"></i>
                <span>Plano de Ensino</span>
            </h5>
            <ul>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/adm/plano-de-ensino/1-periodo.rar" target="_blank">1<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/adm/plano-de-ensino/2-periodo.rar" target="_blank">2<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/adm/plano-de-ensino/3-periodo.rar" target="_blank">3<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/adm/plano-de-ensino/4-periodo.rar" target="_blank">4<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/adm/plano-de-ensino/5-periodo.rar" target="_blank">5<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/adm/plano-de-ensino/6-periodo.rar" target="_blank">6<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/adm/plano-de-ensino/8-periodo.rar" target="_blank">7<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/adm/plano-de-ensino/8-periodo.rar" target="_blank">8<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
            </ul>
            </div>
        </div>
        
    </div>
          
    
</div>
    
<? require 'footer.php'; ?>    

    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.hoverdir.js" type="text/javascript"></script>
    
    
    <script>
        
        $("#btnMenu").click(function(){
        $("#btnMenu").toggleClass("active");
    });
        
    $("#openCloseMap").click(function(){
        $("#mapSite").toggleClass("openMapsFooter");
        
        var openMap = $("#mapSite").hasClass("openMapsFooter");
        if(openMap == true){
            $("#openCloseMap .fa-times").css("display", "inline-block");
            $("#openCloseMap .fa-expand").css("display", "none");
        }else{
            $("#openCloseMap .fa-times").css("display", "none");
            $("#openCloseMap .fa-expand").css("display", "inline-block");
        }
    });
        
        $(function() {
			
				$(' .listDetails > li ').each( function() { $(this).hoverdir(); } );

			});
    $("#menuHome > li").hover(function(){
        if ($(this).hasClass('active')) {
            
        } else {
            $("#menuHome > li").removeClass("active");
        }
    });
     $("#btnServices").click(function(){
        $(".serviceOnline").toggleClass("active");
    });
        $(".btnShare").click(function(){
        $(".standardTitle #redes").toggleClass("active");
    });
    $("#menuHome > li").click(function(){
        $("#menuHome > li").removeClass("active");
        $(this).addClass("active");
    });
        
        $("#btnMenu").click(function(){
        $("#menuHome").toggleClass("active");
        $("body").toggleClass("bodyFix");
    });
        
    
    var widthSite = $(window).width();
    if(widthSite >= 1050){
        $(window).scroll(function (event) {
                var rolado = $(window).scrollTop();
                if (rolado > 100) {
                    $("#barTop").addClass("reduce");
                } else {
                    $("#barTop").removeClass("reduce");
                }
            });
        }
    </script>

    
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.6";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    
    
<script src="https://apis.google.com/js/platform.js" async defer>
        {lang: 'pt-BR'}
    </script>
   <script src="js/jquery.waypoints.min.js"></script>
   <script src="js/jquery.magnific-popup.min.js"></script>  
<script>
(function($) {
    $('.item-wrap a').magnificPopup({
       type:'inline',
       fixedContentPos: false,
       removalDelay: 300,
       showCloseBtn: false,
       mainClass: 'mfp-fade'

    });
    $(document).on('click', '.popup-modal-dismiss', function (e) {
    		e.preventDefault();
    		$.magnificPopup.close();
    });
})(jQuery);
</script>

</body>
</html>
