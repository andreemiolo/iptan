<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>IPTAN</title>
	<meta name="Author" content=""/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon-16x16.png">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/html-reset.css">
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
    <link rel="stylesheet" type="text/css" href="css/coringa.css">
    <link rel="stylesheet" type="text/css" href="css/home.css">
    <link href='https://fonts.googleapis.com/css?family=Pontano+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <script src="js/modernizr.custom.js"></script>
</head>
<body>
    
	<?php 
        require('header.php'); 
    ?>
    
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active">
        <p>Confira o resultado do vestibular IPTAN 2<font face="Arial, Helvetica, sans-serif">º</font> Semestre</p>
      </li>
    <li data-target="#carousel-example-generic" data-slide-to="1">
        <p>Veja o guia passo a passo de rematrícula!</p>
      </li>
    <li data-target="#carousel-example-generic" data-slide-to="2">
        <p>Conheça o Financiamento Privado</p>
      </li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <a href="http://www.iptan.edu.br/novovestibular/" target="_blank"><img src="images/slide.jpg" alt="..."></a>
    </div>
    <div class="item">
      <a href="http://www.iptan.edu.br/arquivos-gerais/financeiro_rematricula_IPTAN_2016_2.pdf" target="_blank"><img src="images/slide2.jpg" alt="..."></a>
      
    </div>
    <div class="item">
      <a href="http://www.iptan.edu.br/creditar/" target="_blank"><img src="images/slide3.jpg" alt="..."></a>
    </div>
  </div>

</div>
    
    
<div id="content">
   
    <!-- NOTÍCIAS -->
    <div id="localNews">
        <div class="centerContent">
            <h1>Notícias</h1>
            
            
            <div class="grid">
              <div class="grid-item imageFeatured" data-colspan="2" data-rowspan="1">
                    <a href="nota1.php">
                        <figure>
                            <img src="images/notas/nota1.jpg">
                        </figure>
                        <div class="textInfoCards">
                            <p>Alunos e professores do curso de Enfermagem do IPTAN atuam na equipe de primeiros socorros do III Jogos Mazarello</p>
                        </div>
                    </a>
                </div>
                <!-- GRID -->
                <div class="grid-item imageFeatured" data-colspan="2" data-rowspan="1">
                    <a href="nota2.php">
                        <figure>
                            <img src="images/notas/nota2.jpg">
                        </figure>
                        <div class="textInfoCards">
                            <p>Militares 11<font face="Arial, Helvetica, sans-serif">º</font> Batalhão de Infantaria de São João del-Rei realizam treinamento médico no IPTAN</p>
                        </div>
                    </a>
                </div>
                <div class="grid-item imageSimple" data-colspan="1" data-rowspan="1">
                <a href="nota3.php">
                        <figure>
                            <img src="images/notas/nota3.jpg">
                        </figure>
                        <div class="textInfoCards">
                            <p>Time de Futsal do Iptan disputa pela primeira vez o (JUMs 2016) Jogos Universitários Mineiros.</p>
                        </div>
                    </a>
                </div>
            </div>
            
            <a href="noticias.php" class="viewMore">Ver mais...</a>
        </div>
    </div>
<!-- NOTÍCIAS -->
    
    
    <div id="localHome">
        <div class="centerContent">
            <div class="areaHome">
                <ul id="listCurse">
                    <li>
                        <a href="administracao.php">
                            <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                            <span>Administração</span>
                        </a>
                    </li>
                    <li>
                        <a href="cienciascontabeis.php">
                            <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                            <span>Ciências Contábeis</span>
                        </a>
                    </li>
                    <li>
                        <a href="direito.php">
                            <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                            <span>Direito</span>
                        </a>
                    </li>
                    <li>
                        <a href="educacaofisica.php">
                            <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                            <span>Educação Física</span>
                        </a>
                    </li>
                    <li>
                        <a href="enfermagem.php">
                            <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                            <span>Enfermagem</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="areaHome">

                 <h1>Calendário Acadêmico</h1>
                <div class="contentCalendar">
            <div class="year">
                2016
            </div>
            <div class="controlDate">
                <i class="fa fa-arrow-left" aria-hidden="true"></i>
                <span>MARÇO</span>
                <i class="fa fa-arrow-right" aria-hidden="true"></i>
            </div>
            
            <ul id="listDates">
                <li>
                    <div class="clickCalendar">
                        <div class="dateNumber">
                            02
                        </div>
                        <div class="siglEvent">
                            <span class="saEvent">SA</span>
                            <span class="sEvent">S</span>
                            <span class="evEvent">EV</span>
                        </div>
                    </div>
                    <ul class="tootipDescription">
                        <i class="fa fa-times-circle" aria-hidden="true"></i>
                        <li>
                            <span class="saEvent">SA</span>
                            <div>
                                <h3>Semana Acadêmica</h3>
                                <p>Hoc vero, quod nimirum exigit legitimam kalendarii restitutionem, iamdiu a Romanis Pontificibus praedecessoribus nostris et saepius tentatum est;</p>
                            </div>
                        </li>
                        <li>
                            <span class="saEvent">SA</span>
                            <div>
                                <h3>Semana Acadêmica</h3>
                                <p>Hoc vero, quod nimirum exigit legitimam kalendarii restitutionem, iamdiu a Romanis Pontificibus praedecessoribus nostris et saepius tentatum est;</p>
                            </div>
                        </li>
                    </ul>
                </li>
                <li>
                    <div class="clickCalendar">
                        <div class="dateNumber">
                            02
                        </div>
                        <div class="siglEvent">
                            <span class="saEvent">SA</span>
                            <span class="sEvent">S</span>
                            <span class="evEvent">EV</span>
                        </div>
                    </div>
                    <ul class="tootipDescription">
                        <i class="fa fa-times-circle" aria-hidden="true"></i>
                        <li>
                            <span class="saEvent">SA</span>
                            <div>
                                <h3>Semana Acadêmica</h3>
                                <p>Hoc vero, quod nimirum exigit legitimam kalendarii restitutionem, iamdiu a Romanis Pontificibus praedecessoribus nostris et saepius tentatum est;</p>
                            </div>
                        </li>
                        <li>
                            <span class="saEvent">SA</span>
                            <div>
                                <h3>Semana Acadêmica</h3>
                                <p>Hoc vero, quod nimirum exigit legitimam kalendarii restitutionem, iamdiu a Romanis Pontificibus praedecessoribus nostris et saepius tentatum est;</p>
                            </div>
                        </li>
                    </ul>
                </li>
                <li>
                    <div class="clickCalendar">
                        <div class="dateNumber">
                            02
                        </div>
                        <div class="siglEvent">
                            <span class="saEvent">SA</span>
                            <span class="sEvent">S</span>
                            <span class="evEvent">EV</span>
                        </div>
                    </div>
                    <ul class="tootipDescription">
                        <i class="fa fa-times-circle" aria-hidden="true"></i>
                        <li>
                            <span class="saEvent">SA</span>
                            <div>
                                <h3>Semana Acadêmica</h3>
                                <p>Hoc vero, quod nimirum exigit legitimam kalendarii restitutionem, iamdiu a Romanis Pontificibus praedecessoribus nostris et saepius tentatum est;</p>
                            </div>
                        </li>
                        <li>
                            <span class="saEvent">SA</span>
                            <div>
                                <h3>Semana Acadêmica</h3>
                                <p>Hoc vero, quod nimirum exigit legitimam kalendarii restitutionem, iamdiu a Romanis Pontificibus praedecessoribus nostris et saepius tentatum est;</p>
                            </div>
                        </li>
                    </ul>
                </li>
                <li>
                    <div class="clickCalendar">
                        <div class="dateNumber">
                            02
                        </div>
                        <div class="siglEvent">
                            <span class="saEvent">SA</span>
                            <span class="sEvent">S</span>
                            <span class="evEvent">EV</span>
                        </div>
                    </div>
                    <ul class="tootipDescription">
                        <i class="fa fa-times-circle" aria-hidden="true"></i>
                        <li>
                            <span class="saEvent">SA</span>
                            <div>
                                <h3>Semana Acadêmica</h3>
                                <p>Hoc vero, quod nimirum exigit legitimam kalendarii restitutionem, iamdiu a Romanis Pontificibus praedecessoribus nostris et saepius tentatum est;</p>
                            </div>
                        </li>
                        <li>
                            <span class="saEvent">SA</span>
                            <div>
                                <h3>Semana Acadêmica</h3>
                                <p>Hoc vero, quod nimirum exigit legitimam kalendarii restitutionem, iamdiu a Romanis Pontificibus praedecessoribus nostris et saepius tentatum est;</p>
                            </div>
                        </li>
                    </ul>
                </li>
                <li>
                    <div class="clickCalendar">
                        <div class="dateNumber">
                            02
                        </div>
                        <div class="siglEvent">
                            <span class="saEvent">SA</span>
                            <span class="sEvent">S</span>
                            <span class="evEvent">EV</span>
                        </div>
                    </div>
                    <ul class="tootipDescription">
                        <i class="fa fa-times-circle" aria-hidden="true"></i>
                        <li>
                            <span class="saEvent">SA</span>
                            <div>
                                <h3>Semana Acadêmica</h3>
                                <p>Hoc vero, quod nimirum exigit legitimam kalendarii restitutionem, iamdiu a Romanis Pontificibus praedecessoribus nostris et saepius tentatum est;</p>
                            </div>
                        </li>
                        <li>
                            <span class="saEvent">SA</span>
                            <div>
                                <h3>Semana Acadêmica</h3>
                                <p>Hoc vero, quod nimirum exigit legitimam kalendarii restitutionem, iamdiu a Romanis Pontificibus praedecessoribus nostris et saepius tentatum est;</p>
                            </div>
                        </li>
                    </ul>
                </li>
                <li>
                    <div class="clickCalendar">
                        <div class="dateNumber">
                            02
                        </div>
                        <div class="siglEvent">
                            <span class="saEvent">SA</span>
                            <span class="sEvent">S</span>
                            <span class="evEvent">EV</span>
                        </div>
                    </div>
                    <ul class="tootipDescription">
                        <i class="fa fa-times-circle" aria-hidden="true"></i>
                        <li>
                            <span class="saEvent">SA</span>
                            <div>
                                <h3>Semana Acadêmica</h3>
                                <p>Hoc vero, quod nimirum exigit legitimam kalendarii restitutionem, iamdiu a Romanis Pontificibus praedecessoribus nostris et saepius tentatum est;</p>
                            </div>
                        </li>
                        <li>
                            <span class="saEvent">SA</span>
                            <div>
                                <h3>Semana Acadêmica</h3>
                                <p>Hoc vero, quod nimirum exigit legitimam kalendarii restitutionem, iamdiu a Romanis Pontificibus praedecessoribus nostris et saepius tentatum est;</p>
                            </div>
                        </li>
                    </ul>
                </li>
                <li>
                    <div class="clickCalendar">
                        <div class="dateNumber">
                            02
                        </div>
                        <div class="siglEvent">
                            <span class="saEvent">SA</span>
                            <span class="sEvent">S</span>
                            <span class="evEvent">EV</span>
                        </div>
                    </div>
                    <ul class="tootipDescription">
                        <i class="fa fa-times-circle" aria-hidden="true"></i>
                        <li>
                            <span class="saEvent">SA</span>
                            <div>
                                <h3>Semana Acadêmica</h3>
                                <p>Hoc vero, quod nimirum exigit legitimam kalendarii restitutionem, iamdiu a Romanis Pontificibus praedecessoribus nostris et saepius tentatum est;</p>
                            </div>
                        </li>
                        <li>
                            <span class="saEvent">SA</span>
                            <div>
                                <h3>Semana Acadêmica</h3>
                                <p>Hoc vero, quod nimirum exigit legitimam kalendarii restitutionem, iamdiu a Romanis Pontificibus praedecessoribus nostris et saepius tentatum est;</p>
                            </div>
                        </li>
                    </ul>
                </li>
                <li>
                    <div class="clickCalendar">
                        <div class="dateNumber">
                            02
                        </div>
                        <div class="siglEvent">
                            <span class="saEvent">SA</span>
                            <span class="sEvent">S</span>
                            <span class="evEvent">EV</span>
                        </div>
                    </div>
                    <ul class="tootipDescription">
                        <i class="fa fa-times-circle" aria-hidden="true"></i>
                        <li>
                            <span class="saEvent">SA</span>
                            <div>
                                <h3>Semana Acadêmica</h3>
                                <p>Hoc vero, quod nimirum exigit legitimam kalendarii restitutionem, iamdiu a Romanis Pontificibus praedecessoribus nostris et saepius tentatum est;</p>
                            </div>
                        </li>
                        <li>
                            <span class="saEvent">SA</span>
                            <div>
                                <h3>Semana Acadêmica</h3>
                                <p>Hoc vero, quod nimirum exigit legitimam kalendarii restitutionem, iamdiu a Romanis Pontificibus praedecessoribus nostris et saepius tentatum est;</p>
                            </div>
                        </li>
                    </ul>
                </li>
                
            </ul>
        </div>
                
            </div>
            
        </div>
    </div>
    
    <div id="localEnade">
        <div class="centerContent">
            <h1 style="color: #fff;">Deixe o IPTAN fazer parte da sua história</h1>
            <div style="color: #fff;" id="contentEnade">
                <div id="textEnade">
                    <!--<h3>São mais de 15 anos oferecendo ensino superior de qualidade</h3>-->
                    <p align="justify">Pensando sempre na sua realização profissional o IPTAN - Instituto de Ensino Superior Presidente Tancredo de Almeida Neves oferece uma formação  de nível superior  para o desempenho de diversas funções. A instituição está comprometida na busca e no aperfeiçoamento contínuo para inserção de seus alunos no mercado de trabalho e o exercício pleno de cidadania. O IPTAN possui o conceito Institucional nota 4 na escala que varia de 1 a 5 estipulada pelo MEC. Buscando sempre melhorar a cada dia, a Instituição está conectada às  rápidas mudanças no campo educacional e atenta às inovações na sua metodologia de ensino.</p>
                </div>
            </div>
            <a href="iptan.php" class="viewMore">Saiba mais</a>
        </div>
    </div>
    
</div>
    
<?php require('footer.php'); ?>    
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
    <script src="js/jquery.nicescroll.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
          $('#listDates').niceScroll({
            autohidemode: 'false',     // Do not hide scrollbar when mouse out
            cursorborderradius: '10px', // Scroll cursor radius
            background: '#E5E9E7',     // The scrollbar rail color
            cursorwidth: '10px',       // Scroll cursor width
            cursorcolor: '#d83d22'     // Scroll cursor color
          });
        });
  </script>
    
    
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.hoverdir.js" type="text/javascript"></script>

<script src="js/jquery.mobile-1.4.5.min.js" type="text/javascript"></script>
    
    <script src="js/responsive-gride.js" type="text/javascript"></script>
    <script type="text/javascript">
    $('.grid').responsivegrid({
        'breakpoints': {
            'desktop' : {
                'range' : '*',
                'options' : {
                    'column' : 5,
                    'gutter' : '10px',
                    'itemHeight' : '100%',
                }
            },
            'tablet-landscape' : {
                'range' : '1000-1200',
                'options' : {
                    'column' : 4,
                    'gutter' : '10px',
                    'itemHeight' : '100%',
                }
            },
            'tablet-portrate' : {
                'range' : '767-1000',
                'options' : {
                    'column' : 3,
                    'gutter' : '5px',
                    'itemHeight' : '100%',
                }
            },
            'mobile' : {
                'range' : '-767',
                'options' : {
                    'column' : 2,
                    'gutter' : '5px',
                    'itemHeight' : '100%',
                }
            },
        }
    });
</script>
    
    
    <script type="text/javascript">
    $("#btnMenu").click(function(){
        $("#btnMenu").toggleClass("active");
    });
    
    // Recarregar página quando a jánela é redimensionada
    $(window).resize(function() {
        location.reload();
    });
        
    $("#listDates li .clickCalendar").click(function(){
        $("#listDates li .clickCalendar").removeClass("active");
        $(this).addClass("active");
        $(".tootipDescription").removeClass("active");
        $(this).next(".tootipDescription").addClass("active");
    });
    $(".tootipDescription").click(function(){
        $(".clickCalendar").removeClass("active");
        $(".tootipDescription").removeClass("active");
    });
    $(".clickCalendar, .tootipDescription").click(function(){
        var viewActive = $(".clickCalendar").hasClass("active");
        if(viewActive == true){
            $(".clickCalendar").css("opacity", "0.5");
        }else{
            $(".clickCalendar").css("opacity", "1");
        }
    });
        
    $("#openCloseMap").click(function(){
        $("#mapSite").toggleClass("openMapsFooter");
        
        var openMap = $("#mapSite").hasClass("openMapsFooter");
        if(openMap == true){
            $("#openCloseMap .fa-times").css("display", "inline-block");
            $("#openCloseMap .fa-expand").css("display", "none");
        }else{
            $("#openCloseMap .fa-times").css("display", "none");
            $("#openCloseMap .fa-expand").css("display", "inline-block");
        }
    });
        
        $(function() {
			
				$(' .listDetails > li ').each( function() { $(this).hoverdir(); } );

			});
        
        
    $("#menuHome > li").hover(function(){
        if ($(this).hasClass('active')) {
            
        } else {
            $("#menuHome > li").removeClass("active");
        }
    });
     
    $("#menuHome > li").click(function(){
        $(this).toggleClass("active");
    });
        
    $("#btnServices").click(function(){
        $(".serviceOnline").toggleClass("active");
    });
        
    $("#btnMenu").click(function(){
        $("#menuHome").toggleClass("active");
        $("body").toggleClass("bodyFix");
    });
        
    
    var widthSite = $(window).width();
    if(widthSite >= 1050){
        $(window).scroll(function (event) {
                var rolado = $(window).scrollTop();
                if (rolado > 100) {
                    $("#barTop").addClass("reduce");
                } else {
                    $("#barTop").removeClass("reduce");
                }
            });
        }
        
        
    //SLIDE
    $('.carousel').carousel();
        
    $(document).ready(function() {
       $(".carousel").swiperight(function() {
          $(this).carousel('prev');
        });
       $(".carousel").swipeleft(function() {
          $(this).carousel('next');
       });
    });
        
    </script>

    
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.6";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

</body>
</html>
