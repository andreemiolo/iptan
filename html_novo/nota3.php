<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>IPTAN</title>
	<meta name="Author" content=""/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon-16x16.png">
    
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/html-reset.css">
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
    <link rel="stylesheet" type="text/css" href="css/blog.css">
    <link href='https://fonts.googleapis.com/css?family=Pontano+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <script src="js/modernizr.custom.js"></script>
</head>
<body>

<? require 'header.php'; ?>      
    
    
<div id="content">
    <ul id="breadcrumb">
        <li>
            <a href="index.php">Home</a>
        </li>
        <li>
            <a href="noticias.php">Notícias</a>
        </li>
        <li>
            <a href="nota3.php">Blog</a>
        </li>
    </ul>
    
    <div class="centerContent">
        
        <div class="barLeft">
            <div id="redes">
                <div class="fb-share-button" 
                    data-href="http://www.your-domain.com/your-page.php" 
                    data-layout="button_count">
                </div>
                <div class="g-plus" data-action="share" data-annotation="bubble"></div>
            </div>
            <h1>Time de Futsal do Iptan disputa pela primeira vez o (JUMs 2016) Jogos Universitários Mineiros.</h1>
            <p align="justify">
                O JUMs  (Jogos Universitários Mineiros) 2016 será disputado entre os dias  26 e 31 de Julho em Uberaba. Pela primeira vez o IPTAN irá participar do torneio com a seleção de Futsal montada pelo técnico Paulo Rogério.
            </p>
            <p align="justify">
                A equipe foi montada a partir da Taça Iptan de Futsal que foi realizada no Instituto Auxiliadora  no mês de maio deste ano, com participação de 14 equipes.  O time também marcou presença no Torneio de Inverno de São João del Rei em julho, onde obteve um bom resultado quase chegando nas semifinais do torneio. Foram selecionados 12 atletas para formar o time que irá disputar o torneio em Uberaba.
            </p>
            <p align="justify">
                Segundo Paulo Rogerio, técnico do time  escalado para os jogos, eles treinam 2 vezes por semana, no Cetan e também no Instituto Auxiliadora. Paulo Rogério está confiante em fazer uma participação marcante no torneio , dada a qualidade técnica  da equipe de Futsal IPTAN. Ainda segundo o técnico, esse é o início de um trabalho e o resultado com o futsal vem a longo prazo. O técnico da equipe do IPTAN acredita em bons resultados no JUMS em Uberaba, "Iremos trabalhar firme para realizar um ótimo torneio", afirmou.
            </p>
            <p align="justify">
                Segundo Rubens Bagni Torres, coordenador do curso de Educação Física do Iptan, o principal objetivo dessa ação é interação intercursos e  também entre as instituições de ensino do Estado. É uma experiência  muito boa para os alunos e uma inserção do Iptan na parte acadêmica esportiva. Além disso é muito importante levar o nome da instituicao para todo o Estado", afirma o coordenador do curso de Educação Física.
            </p>
            <div class="imgsText">
                <img src="images/notas/3A.jpg">
            </div>
            <div class="tagsSearch">
            <h3>Tags</h3>
            <span>FUTSAL</span> <span>IPTAN</span> <span>FUTEBOL</span> <span>ESPORTE</span>
            </div>
        </div>
        <div class="barRight">
            <h5>
                <i class="fa fa-clock-o" aria-hidden="true"></i>
                <span>Recentes</span>
            </h5>
            <ul>
                <li>
                    <a href="nota3.php">
                        <span>14/07/2016</span>
                       <p>Time de Futsal do Iptan disputa pela primeira vez o (JUMs 2016) Jogos Universitários Mineiros.</p> 
                    </a>
                </li>
                <li>
                    <a href="nota2.php">
                        <span>08/07/2016</span>
                       <p>Militares 11<font face="Arial, Helvetica, sans-serif">º</font> Batalhão de Infantaria de São João del-Rei realizam treinamento médico no IPTAN</p> 
                    </a>
                </li>
                <li>
                    <a href="nota1.php">
                        <span>07/07/2016</span>
                       <p>Alunos e professores do curso de Enfermagem do IPTAN atuam na equipe de primeiros socorros do III Jogos Mazarello</p> 
                    </a>
                </li>
            </ul>
        </div>
        
    </div>
    
    
    
    
</div>
    
    
<? require 'footer.php'; ?> 
    
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.hoverdir.js" type="text/javascript"></script>
    
    
    <script>
        
        $("#btnMenu").click(function(){
        $("#btnMenu").toggleClass("active");
    });
        
    $("#openCloseMap").click(function(){
        $("#mapSite").toggleClass("openMapsFooter");
        
        var openMap = $("#mapSite").hasClass("openMapsFooter");
        if(openMap == true){
            $("#openCloseMap .fa-times").css("display", "inline-block");
            $("#openCloseMap .fa-expand").css("display", "none");
        }else{
            $("#openCloseMap .fa-times").css("display", "none");
            $("#openCloseMap .fa-expand").css("display", "inline-block");
        }
    });
        
        $(function() {
			
				$(' .listDetails > li ').each( function() { $(this).hoverdir(); } );

			});
    $("#menuHome > li").hover(function(){
        if ($(this).hasClass('active')) {
            
        } else {
            $("#menuHome > li").removeClass("active");
        }
    });
     $("#btnServices").click(function(){
        $(".serviceOnline").toggleClass("active");
    });
    $("#menuHome > li").click(function(){
        $("#menuHome > li").removeClass("active");
        $(this).addClass("active");
    });
        
        $("#btnMenu").click(function(){
        $("#menuHome").toggleClass("active");
        $("body").toggleClass("bodyFix");
    });
        
    
    var widthSite = $(window).width();
    if(widthSite >= 1050){
        $(window).scroll(function (event) {
                var rolado = $(window).scrollTop();
                if (rolado > 100) {
                    $("#barTop").addClass("reduce");
                } else {
                    $("#barTop").removeClass("reduce");
                }
            });
        }
    </script>

    
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.6";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    
    
<script src="https://apis.google.com/js/platform.js" async defer>
        {lang: 'pt-BR'}
    </script>

</body>
</html>
