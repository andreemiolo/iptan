<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>IPTAN</title>
	<meta name="Author" content=""/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon-16x16.png">
    
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/html-reset.css">
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
    <link rel="stylesheet" type="text/css" href="css/coringa.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/base.css">
    <link rel="stylesheet" type="text/css" href="css/vendor.min.css">
    <link href='https://fonts.googleapis.com/css?family=Pontano+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <script src="js/modernizr.custom.js"></script>
</head>
<body>
    

<? require 'header.php'; ?>      
    
    
<div id="content">
    <ul id="breadcrumb">
        <li>
            <a href="index.php">Home</a>
        </li>
        <li>
            <a href="#">Institucional</a>
        </li>
        <li>
            <a href="#">Administrativo</a>
        </li>
        <li>
            <a href="biblioteca.php">Biblioteca</a>
        </li>
    </ul>
    
    <div class="standardTitle">
        <div class="centerContent">
            <div id="redes">
                <div class="fb-share-button" 
                    data-href="http://www.your-domain.com/your-page.php" 
                    data-layout="button_count">
                </div>
                <div class="g-plus" data-action="share" data-annotation="bubble"></div>
            </div>
            <h1 class="titleDefault">
                <p>Biblioteca</p><br />
            </h1>
            <div class="text">
                <p align="justify">
                    A Biblioteca Central “Prof. Gaio”, do Instituto de Ensino Superior Presidente Tancredo de Almeida Neves -IPTAN, tem como objetivo trabalhar para que a informação se transforme em conhecimento. Unir novas ou antigas teorias e documentações aplicadas, conhecidas ou analisadas, que possam - direta ou indiretamente – contribuir para a evolução do conhecimento e desenvolvimento sócio cultural dos seus usuários e comunidade. Ela teve seu início em fevereiro de 2001, e o seu nome foi escolhido em homenagem a uma grande colaborador do desenvolvimento humano: Antonio Gaio Sobrinho. Ele é um grande colaborador da Biblioteca do Instituto Presidente Tancredo de Almeida Neves com suas doações e obras. Um grande Historiador e Filósofo, uma referência social e cultural de todos que o conhecem e os que ainda vão conhecer; por isso não poderíamos deixar de homenageá-lo, colocando seu nome na Biblioteca do IPTAN.  
                </p>
                <ul class="topicsBiblioteca">
                    <a href="http://186.194.210.79/corpore.net/Source/Bib-Biblioteca/Public/BibFirewall.htm?CodColigada=1&CodFilial=1&CodUnidade=1" target="_blank"><li>
                        <img src="images/biblioteca/bibl-04.svg" ></img>
                        <p class="textbox">Consulta Pública ao Acervo</p>
                    </li></a>
                    <a href="https://itpacaraguaina.bv3.digitalpages.com.br/users/sign_in" target="_blank"><li>
                        <img src="images/biblioteca/bibl-03.svg" ></img>
                        <p class="textbox">Biblioteca Virtual Universitária</p>
                    </li></a>
                    <a href="http://search.ebscohost.com/login.aspx?authtype=ip,uid&profile=ehost" target="_blank"><li>
                        <img src="images/biblioteca/bibl-01.svg" ></img>
                        <p class="textbox">EBSCOhost</p>
                    </li></a>
                    <a href="http://search.ebscohost.com/login.aspx?authtype=ip,uid&profile=dmp" target="_blank"><li>
                        <img src="images/biblioteca/bibl-02.svg" ></img>
                        <p class="textbox">DynaMed Plus</p>
                    </li></a>
                </ul>
            </div>

        <div id="portfolio-wrapper" class="centerContent bgrid-fourth s-bgrid-third tab-bgrid-half">
            <div class="bgrid folio-item">
               <div class="item-wrap">
                  <a href="#modal-01">
                      <img src="images/portfolio/underwater.jpg" alt="Underwater" class="height:400px;">
                     <div class="overlay"></div>                       
                     <div class="portfolio-item-meta">
                              <h5></h5>
                        <p></p>
                           </div> 
                     <div class="link-icon"><i class="fa fa-plus"></i></div>
                  </a>
               </div>
            </div> <!-- item end -->

            <div class="bgrid folio-item">
               <div class="item-wrap">
                  <a href="#modal-02">
                     <img src="images/portfolio/hotel.jpg" alt="Hotel Sign">
                     <div class="overlay">
                      <div class="portfolio-item-meta">
                               <h5></h5>
                           <p></p>
                            </div>
                     </div>
                     <div class="link-icon"><i class="fa fa-plus"></i></div>
                  </a>
               </div>
            </div> <!-- item end -->

            <div class="bgrid folio-item">
               <div class="item-wrap">
                  <a href="#modal-03">
                     <img src="images/portfolio/beetle.jpg" alt="Beetle">                        
                     <div class="overlay">
                      <div class="portfolio-item-meta">
                               <h5></h5>
                           <p></p>
                            </div>
                     </div>
                     <div class="link-icon"><i class="fa fa-plus"></i></div>
                  </a>
               </div>
            </div> <!-- item end -->

            <div class="bgrid folio-item">
               <div class="item-wrap">
                  <a href="#modal-04">
                     <img src="images/portfolio/banjo-player.jpg" alt="Banjo Player">
                     <div class="overlay">
                      <div class="portfolio-item-meta">
                               <h5></h5>
                           <p></p>
                            </div>
                     </div>
                     <div class="link-icon"><i class="fa fa-plus"></i></div>
                  </a>
               </div>
            </div> <!-- item end -->

         </div> <!-- end portfolio-wrapper -->

        
        </div>
    </div>
    
    
    <div class="centerContent">
        
        <div class="barLeft">
            <p align="justify">
                O acervo inicial da Biblioteca já estava sendo preparado mesmo antecedendo a sua fundação , tendo em vista a necessidade e a preocupação da Instituição com a formação de valores técnicos e documentais para a Biblioteca. O controle documental é feito através de sistema computadorizado visando sobretudo, a eficiência na recuperação da informação. Nos anos que se seguiram, a biblioteca veio a se estruturar para cumprir propósitos instituicionais e fazer frente ao crescimento da instituição e do corpo docente e discente. A partir de 2003, com a nova mantenedora, a biblioteca recebeu especial atenção com novos investimentos na atualização do acervo físico para atender a demanda de cursos existentes e novos cursos, maior abertura do acervo para uso local, na instituição ou mesmo fora dela, através de controles apropriados de software, etc. 
            </p>
            <p align="justify">
                Sendo assim é importante salientar que o acervo é criteriosamente selecionado e aplicado às necessidades da Instituição e de seu público-alvo. Esta característica revela a preocupação institucional com a qualidade e a eficácia do material disponível. A Biblioteca além de possuir um rico acervo atende também a normas internacionais exigidas, além de todas as exigências federais do MEC - Ministério da Educação e Cultura e necessidades de sua comunidade acadêmica e externa. 
            </p>
            <p align="justify">
                A Biblioteca trabalha com maturidade, inclusão e acessibilidade de todos para todas as áreas do conhecimento e desenvolvimento integral humano.
            </p>
            <!--<div class="tagsSearch">
                <h3>Tags</h3>
                <span>CURSO</span> <span>IPTAN</span> <span>PALESTRA</span> <span>EXTENSÃO</span> <span>VESTIBULAR</span>
            </div>-->
        </div>
        <div class="barRight">
            <div class="blockRight">
                <h5>
                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                    <span>Horário</span>
                </h5>
                <ul>
                    <li>
                        <p style="font-size:13px">De segunda à sexta de<br> 08h às 21h45min</p>
                    </li>
                </ul>
            </div>
            
            <div class="blockRight">
                <h5>
                    <i class="fa fa-commenting-o" aria-hidden="true"></i>
                    <span>Contato</span>
                </h5>
                <ul>
                    <li>
                        <p style="font-size:13px"><strong>Telefone:</strong> 3379-2725 / ramal 215</p>
                    </li>
                    <li>
                        <p style="font-size:13px"><strong>E-mail:</strong> biblioteca@iptan.edu.br</p>
                    </li>
                </ul>
            </div>
        </div>
        
    </div>
    
    
    
</div>
    
    
<? require 'footer.php'; ?> 
    
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.hoverdir.js" type="text/javascript"></script>
    
    
    <script>
        
        $("#btnMenu").click(function(){
        $("#btnMenu").toggleClass("active");
    });
        
    $("#openCloseMap").click(function(){
        $("#mapSite").toggleClass("openMapsFooter");
        
        var openMap = $("#mapSite").hasClass("openMapsFooter");
        if(openMap == true){
            $("#openCloseMap .fa-times").css("display", "inline-block");
            $("#openCloseMap .fa-expand").css("display", "none");
        }else{
            $("#openCloseMap .fa-times").css("display", "none");
            $("#openCloseMap .fa-expand").css("display", "inline-block");
        }
    });
        
        $(function() {
			
				$(' .listDetails > li ').each( function() { $(this).hoverdir(); } );

			});
    $("#menuHome > li").hover(function(){
        if ($(this).hasClass('active')) {
            
        } else {
            $("#menuHome > li").removeClass("active");
        }
    });
     $("#btnServices").click(function(){
        $(".serviceOnline").toggleClass("active");
    });
    $("#menuHome > li").click(function(){
        $("#menuHome > li").removeClass("active");
        $(this).addClass("active");
    });
        
        $("#btnMenu").click(function(){
        $("#menuHome").toggleClass("active");
        $("body").toggleClass("bodyFix");
    });
        
    
    var widthSite = $(window).width();
    if(widthSite >= 1050){
        $(window).scroll(function (event) {
                var rolado = $(window).scrollTop();
                if (rolado > 100) {
                    $("#barTop").addClass("reduce");
                } else {
                    $("#barTop").removeClass("reduce");
                }
            });
        }
    </script>

    
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.6";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    
    
<script src="https://apis.google.com/js/platform.js" async defer>
        {lang: 'pt-BR'}
    </script>
   <script src="js/jquery.waypoints.min.js"></script>
   <script src="js/jquery.magnific-popup.min.js"></script>  
<script>
(function($) {
    $('.item-wrap a').magnificPopup({
       type:'inline',
       fixedContentPos: false,
       removalDelay: 300,
       showCloseBtn: false,
       mainClass: 'mfp-fade'

    });
    $(document).on('click', '.popup-modal-dismiss', function (e) {
            e.preventDefault();
            $.magnificPopup.close();
    });
})(jQuery);
</script>
</body>
</html>
