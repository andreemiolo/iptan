<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>IPTAN</title>
	<meta name="Author" content=""/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon-16x16.png">
    
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/html-reset.css">
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
    <link rel="stylesheet" type="text/css" href="css/cursos.css">
    <link href='https://fonts.googleapis.com/css?family=Pontano+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <script src="js/modernizr.custom.js"></script>
</head>
<body>
    


<? require 'header.php'; ?>    

    
<div id="content">
   


    <div id="localNews">
        <div class="centerContent">
            <h1>Cursos</h1>
            <!--<div id="category">
                <select class="form-control">
                    <option>Exibir por</option>
                    <option>Opção</option>
                    <option>Opção</option>  
                </select>
            </div>-->
            
            <div class="grid" id="cursos">
              <div class="grid-item imageSimple" data-colspan="1" data-rowspan="1">
                <a href="administracao.php">
                        <p>Administração</p>
                        <figure>
                            <img src="images/news.jpg">
                        </figure>
                    </a>
                </div>
                <!-- GRID -->
                <div class="grid-item imageSimple" data-colspan="1" data-rowspan="1">
                <a href="cienciascontabeis.php">
                    <p>Ciências Contábeis gdsfg gfg </p>
                        <figure>
                            <img src="images/02-thumb-crs.jpg">
                        </figure>
                        
                    </a>
                </div>
                <div class="grid-item imageSimple" data-colspan="1" data-rowspan="1">
                <a href="direito.php">
                        <figure>
                            <img src="images/03-thumb-crs.jpg">
                        </figure>
                        
                    </a>
                </div>
                <div class="grid-item imageSimple" data-colspan="1" data-rowspan="1">
                    <a href="educacaofisica.php">
                        <figure>
                            <img src="images/04-thumb-crs.jpg">
                        </figure>
                        
                    </a>
                </div>
                <div class="grid-item imageSimple" data-colspan="1" data-rowspan="1">
                <a href="enfermagem.php">
                        <figure>
                            <img src="images/05-thumb-crs.jpg">
                        </figure>
                        
                    </a>
                </div>
                <div class="grid-item imageSimple" data-colspan="1" data-rowspan="1">
                <a href="engenhariacivil.php">
                        <figure>
                            <img src="images/08-thumb-crs.jpg">
                        </figure>
                        
                    </a>
                </div>
                <div class="grid-item imageSimple" data-colspan="1" data-rowspan="1">
                <a href="engenhariadeproducao.php">
                        <figure>
                            <img src="images/09-thumb-crs.jpg">
                        </figure>
                        
                    </a>
                </div>
                <div class="grid-item imageSimple" data-colspan="1" data-rowspan="1">
                <a href="medicina.php">
                        <figure>
                            <img src="images/07-thumb-crs.jpg">
                        </figure>
                        
                    </a>
                </div>
                <div class="grid-item imageSimple" data-colspan="1" data-rowspan="1">
                <a href="odontologia.php">
                        <figure>
                            <img src="images/10-thumb-crs.jpg">
                        </figure>
                        
                    </a>
                </div>
                <div class="grid-item imageSimple" data-colspan="1" data-rowspan="1">
                <a href="pedagogia.php">
                        <figure>
                            <img src="images/06-thumb-crs.jpg">
                        </figure>
                        
                    </a>
                </div>
                
            </div>
            
        </div>
    </div>
<!-- NOTÍCIAS -->
    
    
    
    
</div>
    

    
    
<? require 'footer.php'; ?>    

    
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.hoverdir.js" type="text/javascript"></script>
    
    <script src="js/responsive-gride.js" type="text/javascript"></script>
    <script type="text/javascript">
    $('.grid').responsivegrid({
        'breakpoints': {
            'desktop' : {
                'range' : '*',
                'options' : {
                    'column' : 5,
                    'gutter' : '10px',
                    'itemHeight' : '100%',
                }
            },
            'tablet-landscape' : {
                'range' : '1000-1200',
                'options' : {
                    'column' : 4,
                    'gutter' : '10px',
                    'itemHeight' : '100%',
                }
            },
            'tablet-portrate' : {
                'range' : '767-1000',
                'options' : {
                    'column' : 3,
                    'gutter' : '5px',
                    'itemHeight' : '100%',
                }
            },
            'mobile' : {
                'range' : '-767',
                'options' : {
                    'column' : 2,
                    'gutter' : '5px',
                    'itemHeight' : '100%',
                }
            },
        }
    });
</script>
    
    <script>
        $("#btnMenu").click(function(){
        $("#btnMenu").toggleClass("active");
    });
        
    $("#listDates li .clickCalendar").click(function(){
        $("#listDates li .clickCalendar").removeClass("active");
        $(this).addClass("active");
        $(".tootipDescription").removeClass("active");
        $(this).next(".tootipDescription").addClass("active");
    });
    $(".tootipDescription").click(function(){
        $(".clickCalendar").removeClass("active");
        $(".tootipDescription").removeClass("active");
    });
    $(".clickCalendar, .tootipDescription").click(function(){
        var viewActive = $(".clickCalendar").hasClass("active");
        if(viewActive == true){
            $(".clickCalendar").css("opacity", "0.5");
        }else{
            $(".clickCalendar").css("opacity", "1");
        }
    });
        
    $("#openCloseMap").click(function(){
        $("#mapSite").toggleClass("openMapsFooter");
        
        var openMap = $("#mapSite").hasClass("openMapsFooter");
        if(openMap == true){
            $("#openCloseMap .fa-times").css("display", "inline-block");
            $("#openCloseMap .fa-expand").css("display", "none");
        }else{
            $("#openCloseMap .fa-times").css("display", "none");
            $("#openCloseMap .fa-expand").css("display", "inline-block");
        }
    });
        
        $(function() {
			
				$(' .listDetails > li ').each( function() { $(this).hoverdir(); } );

			});
    $("#menuHome > li").hover(function(){
        if ($(this).hasClass('active')) {
            
        } else {
            $("#menuHome > li").removeClass("active");
        }
    });
     $("#btnServices").click(function(){
        $(".serviceOnline").toggleClass("active");
    });
    $("#menuHome > li").click(function(){
        $("#menuHome > li").removeClass("active");
        $(this).addClass("active");
    });
        
    $("#btnMenu").click(function(){
        $("#menuHome").toggleClass("active");
        $("body").toggleClass("bodyFix");
    });
        
    
    var widthSite = $(window).width();
    if(widthSite >= 1050){
        $(window).scroll(function (event) {
                var rolado = $(window).scrollTop();
                if (rolado > 100) {
                    $("#barTop").addClass("reduce");
                } else {
                    $("#barTop").removeClass("reduce");
                }
            });
        }
        
        
        
        
    //SLIDE
    $('.carousel').carousel();
    </script>

    
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.6";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

</body>
</html>
