<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>IPTAN</title>
	<meta name="Author" content=""/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon-16x16.png">
    
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/html-reset.css">
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
    <link rel="stylesheet" type="text/css" href="css/coringa.css">
    <link href='https://fonts.googleapis.com/css?family=Pontano+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <script src="js/modernizr.custom.js"></script>
</head>
<body>
<!--   
<a href="#" class="btnCourse">
    Quero saber mais sobre este curso
</a>
-->
<? require 'header.php'; ?>      
    
<div id="content">
    <ul id="breadcrumb">
        <li>
            <a href="index.php">Home</a>
        </li>
        <li>
            <a href="#">Institucional</a>
        </li>
        <li>
            <a href="#">Administrativo</a>
        </li>
        <li>
            <a href="tecnologiadainformacao.php">Tecnologia da Informação</a>
        </li>
    </ul>
    
    <div class="standardTitle">
        <div class="centerContent">
            <div id="redes">
                <div class="fb-share-button" 
                    data-href="http://www.your-domain.com/your-page.php" 
                    data-layout="button_count">
                </div>
                <div class="g-plus" data-action="share" data-annotation="bubble"></div>
            </div>
            <h1 class="titleDefault">
                <p>Tecnologia da Informação</p><br />
            </h1>
            <div class="text">
                <p align="justify">
                    O setor de tecnologia da informação tem como atribuições executar e gerenciar o planejamento, especificação, desenvolvimento, implantação, operação e a manutenção de serviços, sistemas de informação e infra–estrutura de tecnologia da informação e telecomunicação, desenvolver conhecimentos e atividades, através de projetos, convênios e parcerias, na busca de soluções eficazes e eficientes na área de tecnologia da informação e telecomunicação, prestar serviços de atendimento e suporte à colaboradores para a plena utilização dos recursos computacionais de sistemas de informação e telecomunicação definir política de uso de softwares e hardwares, analisar e definir produtos para rede lógica e física, planejar e promover capacitação de colaboradores promover e estimular para os departamentos o uso racional e econômico dos recursos de informática da Instituição, promover a evolução do pessoal de informática e dos recursos de hardware e software, organizar e participar de organizações para a democratização e racionalização da informática e telecomunicações na representação da instituição, manutenção nos equipamentos e atendimento (suporte operacional) aos usuários de informática, manter em dia e atualizado as rotinas de cópias de segurança dos dados nos equipamentos servidores e unidades de bkp, executar serviços de infra-estrutura de comunicação de dados(cabeamentos e conectorização de redes), além de controlar as atividades relacionadas a segurança e comunicação de dados, desenvolvimento de novos sistemas (programas), e manutenção dos sistemas existentes na instituição, inclusive Web Site e suporte a usuários na utilização de softwares.
                </p>
            </div>            

        </div>
    </div>
    <div class="centerContent">
         <!--
         <h1 class="titleDefault">
                <p>Secretaria de Registros e Controle Acadêmicos</p>
            </h1>
        <div class="barLeft">
            <p>
                A Secretaria de Registros e Controle Acadêmicos é um órgão de apoio e está subordinada a Diretoria de Graduação. É responsável pelo controle, verificação, registro e arquivamento da documentação da vida acadêmica do aluno, desde o seu ingresso até a conclusão e/ou expedição do diploma. Cabe à Secretaria de Registros e Controle Acadêmicos o atendimento direto aos alunos, no que se refere aos procedimentos acadêmicos, além da emissão de documentação escolar (declarações, históricos, atestado de conclusão e planos de ensino).
            </p>  
        </div>
        -->                
        <div class="barRight">
            <!--
            <h5>
                <i class="fa fa-link" aria-hidden="true"></i>
                <span>Acesso</span>
            </h5>
            <ul>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-setores/secretaria/edital_rematricula.pdf">Edital Rematrícula</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-setores/secretaria/regime-dependecia.pdf">Regime Dependência</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-setores/secretaria/2-chamada-avaliacao.pdf">2ª Chamada de Avaliação</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-setores/secretaria/solicitacao-de-documentos.pdf">Prazos para solicitação de documentos</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-setores/secretaria/informativo2015.pdf">Informativo 2015</a>
                </li>
            </ul>
            -->
            <ul>
                <li>
                    
                </li>
            </ul>
             <h5>
                <i class="fa fa-clock-o" aria-hidden="true"></i>
                <span>Horário</span>
            </h5>
            <ul>
                <li>
                    <p style="font-size:13px">De segunda à sexta de<br> 08h às 21h30min</p>
                </li>
            </ul>
            <h5>
                <i class="fa fa-commenting-o" aria-hidden="true"></i>
                <span>Contato</span>
            </h5>
            <ul>
                <li>
                    <p style="font-size:13px"><strong>Telefone:</strong> 3379-2725 / ramal 214</p>
                </li>
                <li>
                    <p style="font-size:13px"><strong>E-mail:</strong> ti@iptan.edu.br</p>
                </li>
            </ul>            
        </div>
        
    </div>    
    
</div>
    
    <? require 'footer.php'; ?> 
    
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.hoverdir.js" type="text/javascript"></script>
    
    
    <script>
        
        $("#btnMenu").click(function(){
        $("#btnMenu").toggleClass("active");
    });
        
    $("#openCloseMap").click(function(){
        $("#mapSite").toggleClass("openMapsFooter");
        
        var openMap = $("#mapSite").hasClass("openMapsFooter");
        if(openMap == true){
            $("#openCloseMap .fa-times").css("display", "inline-block");
            $("#openCloseMap .fa-expand").css("display", "none");
        }else{
            $("#openCloseMap .fa-times").css("display", "none");
            $("#openCloseMap .fa-expand").css("display", "inline-block");
        }
    });
        
        $(function() {
			
				$(' .listDetails > li ').each( function() { $(this).hoverdir(); } );

			});
    $("#menuHome > li").hover(function(){
        if ($(this).hasClass('active')) {
            
        } else {
            $("#menuHome > li").removeClass("active");
        }
    });
     $("#btnServices").click(function(){
        $(".serviceOnline").toggleClass("active");
    });
    $("#menuHome > li").click(function(){
        $("#menuHome > li").removeClass("active");
        $(this).addClass("active");
    });
        
        $("#btnMenu").click(function(){
        $("#menuHome").toggleClass("active");
        $("body").toggleClass("bodyFix");
    });
        
    
    var widthSite = $(window).width();
    if(widthSite >= 1050){
        $(window).scroll(function (event) {
                var rolado = $(window).scrollTop();
                if (rolado > 100) {
                    $("#barTop").addClass("reduce");
                } else {
                    $("#barTop").removeClass("reduce");
                }
            });
        }
    </script>

    
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.6";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    
    
<script src="https://apis.google.com/js/platform.js" async defer>
        {lang: 'pt-BR'}
    </script>

</body>
</html>
