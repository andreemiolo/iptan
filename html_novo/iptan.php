<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>IPTAN</title>
	<meta name="Author" content=""/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon-16x16.png">
    
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/html-reset.css">
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
    <link rel="stylesheet" type="text/css" href="css/coringa.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/base.css">
    <link rel="stylesheet" type="text/css" href="css/vendor.min.css">
    <!-- <link rel="stylesheet" type="text/css" href="css/home.css"> -->
    <link href='https://fonts.googleapis.com/css?family=Pontano+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <script src="js/modernizr.custom.js"></script>
</head>
<body>
    
<a href="cursos.php" class="btnCourse">
    <img src="images/img-lateral.jpg">
</a>

<? require 'header.php'; ?>      
    
    
<div id="content">
    <ul id="breadcrumb">
        <li>
            <a href="index.php">Home</a>
        </li>
        <li>
            <a href="#">Institucional</a>
        </li>
        <li>
            <a href="iptan.php">O IPTAN</a>
        </li>
    </ul>
    
    <div class="standardTitle oiptan">
        <div class="centerContent">
            <div id="redes">
                <div class="fb-share-button" 
                    data-href="http://www.your-domain.com/your-page.php" 
                    data-layout="button_count">
                </div>
                <div class="g-plus" data-action="share" data-annotation="bubble"></div>
            </div>
            <h1 class="titleDefault">
                <p>O IPTAN</p>
                <br />
            </h1>
            <div class="text">
                <p align="justify">
                    Pensando sempre na sua realização profissional o IPTAN - Instituto de Ensino Superior Presidente Tancredo de Almeida Neves oferece uma formação  de nível superior  para o desempenho de diversas funções. A instituição está comprometida na busca e no aperfeiçoamento contínuo para inserção de seus alunos no mercado de trabalho e o exercício pleno de cidadania. O IPTAN possui o conceito Institucional nota 4 na escala que varia de 1 a 5 estipulada pelo MEC. Buscando sempre melhorar a cada dia, a Instituição está conectada às  rápidas mudanças no campo educacional e atenta às inovações na sua metodologia de ensino.
                </p><br />
            </div>
            
            <div class="movie">
            <iframe width="100%" height="415" src="https://www.youtube.com/embed/5SlbA3nFpXI" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
    
    <div class="conteudoInst centerContent">
        
        <div class="text">
            <div class="row">
                <div class="txtrow col-md-12">
                    <h1 class="titleDefault">
                        <p>Nossa História</p><br />
                    </h1>
                    <p align="justify">
                        Criada com intuito de oferecer um ensino de qualidade, com programas de pesquisa e extensão e professores com qualificação em mestrado e doutorado o IPTAN homenageia o estadista e ilustre são-joanense, Tancredo de Almeida Neves dando à Instituição o seu nome.<br /><br />
                        Atualmente, o IPTAN oferece os cursos de graduação em Administração, Ciências Contábeis, Direito, Educação Física Bacharelado e Licenciatura, Enfermagem, Engenharia Civil, Engenharia de Produção, Medicina  Odontologia e Pedagogia.<br /><br />
                        O IPTAN é uma Instituição Particular de Ensino Superior, localizada na cidade de São João del-Rei, Estado de Minas Gerais, credenciada pelo Ministério da Educação, conforme a Portaria Nº 2.065, de 21 de dezembro de 2000.
                	</p>
                </div>
            </div>
            <div class="row">
                    <div class="txtrow col-md-4">
                    <h1 class="titleDefault">
                        <p>Missão</p><br />
                    </h1>
                        <p align="justify"><b>Promover o aprendizado inovador</b>, oportunizando a prática da pesquisa e extensão visando à permanente formação de cidadãos críticos, criativos e éticos.</p>
                </div>
                <div class="txtrow col-md-4">
                    <h1 class="titleDefault">
                        <p>Visão</p><br />
                    </h1>
                        <p align="justify"><b>Ser uma unidade de ensino de referência</b> imbuída dos propósitos de responsabilidade social e ambiental.</p>
                </div>
                <div class="txtrow col-md-4">
                    <h1 class="titleDefault">
                        <p>Valores</p><br />
                    </h1>
                        <p align="justify"><b>Indissociabilidade</b> entre ensino, pesquisa e extensão; <b>Parceria</b> interinstitucional; <b>Transparência</b> e conduta nas ações institucionais; <b>Avaliação</b> permanente de processos e gestão; <b>Norteamento</b> das ações com foco na dignidade humana.</p>
                </div>
            </div>
        </div>
        
    </div>
    
    <div class="listImgOptionIptan">
        <div class="centerContent">
            <h1>Corpo Diretivo</h1>
            <ul>
                <li>
                    <a href="#">
                        <figure>
                        <img src="images/Ricardo_Viegas_m.jpg">
                        </figure>
                    
                    <h3>Ricardo Assunção Viegas</h3>
                    <p>Diretor Geral</p>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <figure>
                        <img src="images/Maria_Tereza_Lima_m.jpg">
                        </figure>
                    
                    <h3>Maria Tereza Gomes de Almeida Lima</h3>
                    <p>Diretora de Graduação</p>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <figure>
                        <img src="images/Heberth_Souza_m.jpg">
                        </figure>
                    
                    <h3>Heberth Paulo de Souza</h3>
                    <p>Diretor de Ensino Pesquisa e Extensão</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    
    <div class="listLinks">
        <div class="centerContent">
            <h1 class="titleDefalt">Estrutura</h1>
            <div id="portfolio-wrapper" class="centerContent bgrid-fourth s-bgrid-third tab-bgrid-half">


            <div class="bgrid folio-item">
               <div class="item-wrap">
                  <a href="#modal-01">
                      <img src="images/portfolio/underwater.jpg" alt="Underwater" class="height:400px;">
                     <div class="overlay"></div>                       
                     <div class="portfolio-item-meta">
                              <h5></h5>
                        <p></p>
                           </div> 
                     <div class="link-icon"><i class="fa fa-plus"></i></div>
                  </a>
               </div>
            </div> <!-- item end -->

            <div class="bgrid folio-item">
               <div class="item-wrap">
                  <a href="#modal-02">
                     <img src="images/portfolio/hotel.jpg" alt="Hotel Sign">
                     <div class="overlay">
                      <div class="portfolio-item-meta">
                               <h5> </h5>
                           <p></p>
                            </div>
                     </div>
                     <div class="link-icon"><i class="fa fa-plus"></i></div>
                  </a>
               </div>
            </div> <!-- item end -->

            <div class="bgrid folio-item">
               <div class="item-wrap">
                  <a href="#modal-03">
                     <img src="images/portfolio/beetle.jpg" alt="Beetle">                        
                     <div class="overlay">
                      <div class="portfolio-item-meta">
                               <h5></h5>
                           <p></p>
                            </div>
                     </div>
                     <div class="link-icon"><i class="fa fa-plus"></i></div>
                  </a>
               </div>
            </div> <!-- item end -->

            <div class="bgrid folio-item">
               <div class="item-wrap">
                  <a href="#modal-04">
                     <img src="images/portfolio/banjo-player.jpg" alt="Banjo Player">
                     <div class="overlay">
                      <div class="portfolio-item-meta">
                               <h5></h5>
                           <p></p>
                            </div>
                     </div>
                     <div class="link-icon"><i class="fa fa-plus"></i></div>
                  </a>
               </div>
            </div> <!-- item end -->

            <div class="bgrid folio-item">
               <div class="item-wrap">
                  <a href="#modal-05">
                   <img src="images/portfolio/lighthouse.jpg" alt="Lighthouse">
                     <div class="overlay">
                      <div class="portfolio-item-meta">
                               <h5></h5>
                           <p></p>
                            </div>
                     </div>
                     <div class="link-icon"><i class="fa fa-plus"></i></div>
                  </a>
               </div>
            </div> <!-- item end -->

            <div class="bgrid folio-item">
               <div class="item-wrap">
                  <a href="#modal-06">
                     <img src="images/portfolio/girl.jpg" alt="Girl Stuff">
                     <div class="overlay">
                      <div class="portfolio-item-meta">
                               <h5></h5>
                           <p></p>
                            </div>
                     </div>
                     <div class="link-icon"><i class="fa fa-plus"></i></div>
                  </a>
               </div>
            </div> <!-- item end -->

            <div class="bgrid folio-item">
               <div class="item-wrap">
                  <a href="#modal-07">                        
                     <img src="images/portfolio/coffee.jpg" alt="Coffee Cup">                       
                     <div class="overlay">
                      <div class="portfolio-item-meta">
                               <h5></h5>
                           <p></p>
                            </div>
                     </div>
                     <div class="link-icon"><i class="fa fa-plus"></i></div>
                  </a>
               </div>
            </div> <!-- item end -->

            <div class="bgrid folio-item">
               <div class="item-wrap">
                  <a href="#modal-08">
                     <img src="images/portfolio/judah.jpg" alt="Judah">
                     <div class="overlay">
                        <div class="portfolio-item-meta">
                                  <h5></h5>
                           <p></p>
                               </div>
                     </div>
                     <div class="link-icon"><i class="fa fa-plus"></i></div>                     
                  </a>
               </div>
            </div>  <!-- item end -->

         </div> <!-- end portfolio-wrapper -->

         
        
        </div>
    </div>
    <style type="text/css">
		#localEnade{
			background: url(images/enade.jpg) no-repeat center #184463;
			background-size: cover;
			padding:30px 0 30px 0;
		}
		#contentEnade{
			width: 100%;
			margin-top: 30px;
			display: table;
		}
		#contentEnade #textEnade{
			width: 100%;
			line-height: 21px;
		}
		#localEnade p{
			font-size: 18px;
		}
		.viewMore{
			width: 200px;
			padding: 10px 0;
			margin: 20px auto;
			font-size: 20px;
			display: table;
			text-align: center;
			color: #fff;
			background: #d83d22;
			text-decoration: none;
			/*para Firefox*/
			-moz-border-radius: 5px;
			/*para Safari y Chrome*/
			-webkit-border-radius: 5px;
			/* para Opera */
			border-radius: 5px;
		}
		.viewMore:hover{
			color: #fff;
			text-decoration: none;
		}					
	</style>
    <div id="localEnade">
        <div class="centerContent">
            <center><h1 style="color: #fff;">Regimento e Plano de Desenvolvimento Institucional</h1></center>
            <div style="color: #fff;" id="contentEnade">
                <div id="textEnade">
                    <p></p>
                </div>
            </div>
            <a href="http://www.iptan.edu.br/arquivos-gerais/regimento_iptan.pdf" class="viewMore" target="_blank">Regimento</a>
            <a href="http://www.iptan.edu.br/arquivos-gerais/pdi-iptan-2012-2016.pdf" class="viewMore" target="_blank">PDI</a>
        </div>
    </div>    
    
</div>
    
    
<? require 'footer.php'; ?> 
    
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.hoverdir.js" type="text/javascript"></script>
    
    
    <script>
        
        $("#btnMenu").click(function(){
        $("#btnMenu").toggleClass("active");
    });
        
    $("#openCloseMap").click(function(){
        $("#mapSite").toggleClass("openMapsFooter");
        
        var openMap = $("#mapSite").hasClass("openMapsFooter");
        if(openMap == true){
            $("#openCloseMap .fa-times").css("display", "inline-block");
            $("#openCloseMap .fa-expand").css("display", "none");
        }else{
            $("#openCloseMap .fa-times").css("display", "none");
            $("#openCloseMap .fa-expand").css("display", "inline-block");
        }
    });
        
        $(function() {
			
				$(' .listDetails > li ').each( function() { $(this).hoverdir(); } );

			});
    $("#menuHome > li").hover(function(){
        if ($(this).hasClass('active')) {
            
        } else {
            $("#menuHome > li").removeClass("active");
        }
    });
     $("#btnServices").click(function(){
        $(".serviceOnline").toggleClass("active");
    });
    $("#menuHome > li").click(function(){
        $("#menuHome > li").removeClass("active");
        $(this).addClass("active");
    });
        
        $("#btnMenu").click(function(){
        $("#menuHome").toggleClass("active");
        $("body").toggleClass("bodyFix");
    });
        
    
    var widthSite = $(window).width();
    if(widthSite >= 1050){
        $(window).scroll(function (event) {
                var rolado = $(window).scrollTop();
                if (rolado > 100) {
                    $("#barTop").addClass("reduce");
                } else {
                    $("#barTop").removeClass("reduce");
                }
            });
        }
    </script>

    
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.6";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    
    
<script src="https://apis.google.com/js/platform.js" async defer>
        {lang: 'pt-BR'}
    </script>

<script src="https://apis.google.com/js/platform.js" async defer>
        {lang: 'pt-BR'}
    </script>
   <script src="js/jquery.waypoints.min.js"></script>
   <script src="js/jquery.magnific-popup.min.js"></script>  
<script>
(function($) {
    $('.item-wrap a').magnificPopup({
       type:'inline',
       fixedContentPos: false,
       removalDelay: 300,
       showCloseBtn: false,
       mainClass: 'mfp-fade'

    });
    $(document).on('click', '.popup-modal-dismiss', function (e) {
            e.preventDefault();
            $.magnificPopup.close();
    });
})(jQuery);
</script>

</body>
</html>
