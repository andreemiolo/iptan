<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>IPTAN</title>
	<meta name="Author" content=""/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon-16x16.png">
    
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/html-reset.css">
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
    <link rel="stylesheet" type="text/css" href="css/noticias.css">
    <link href='https://fonts.googleapis.com/css?family=Pontano+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <script src="js/modernizr.custom.js"></script>
</head>
<body>
    


<? require 'header.php'; ?>      
    
<div id="content">
   

<!-- NOTÍCIAS -->
    <div id="localNews">
        <div class="centerContent">
            <h1>Notícias</h1>
            <!--<div id="category">
                <select class="form-control">
                    <option>Exibir por</option>
                    <option>Institucional</option>
                    <option>Extensão</option> 
                    <option>Pesquisa</option> 
                    <option>Vestibular</option> 
                    <option>Graduação</option> 
                    <option>EAD</option> 
                </select>
            </div>-->
                <div class="grid cardsLinks">
              <div class="grid-item imageFeatured" data-colspan="2" data-rowspan="1">
                <a href="nota3.php">
                        <figure>
                            <img src="images/notas/nota3.jpg">
                        </figure>
                        <div class="textInfoCards">
                            <p>Time de Futsal do Iptan disputa pela primeira vez o (JUMs 2016) Jogos Universitários Mineiros.</p>
                        </div>
                    </a>
                </div>
                <!-- GRID -->
                <div class="grid-item imageSimple" data-colspan="1" data-rowspan="1">
                <a href="nota2.php">
                        <figure>
                            <img src="images/notas/nota2.jpg">
                        </figure>
                        <div class="textInfoCards">
                            <p>Militares 11º Batalhão de Infantaria de São João del-Rei realizam treinamento médico no IPTAN</p>
                        </div>
                    </a>
                </div>
                <div class="grid-item imageSimple" data-colspan="1" data-rowspan="1">
                <a href="nota1.php">
                        <figure>
                            <img src="images/notas/nota1.jpg">
                        </figure>
                        <div class="textInfoCards">
                            <p>Alunos e professores do curso de Enfermagem do IPTAN atuam na equipe de primeiros socorros do III Jogos Mazarello</p>
                        </div>
                    </a>
                </div>
            </div>
            
            
            <ul id="listOld">
                <h3>Notícias Anteriores</h3>
                <li>
                    <a href="nota3.php">Time de Futsal do Iptan disputa pela primeira vez o (JUMs 2016) Jogos Universitários Mineiros.</a>
                    <span>14/07/2016</span>
                </li>
                <li>
                    <a href="nota2.php">Militares 11º Batalhão de Infantaria de São João del-Rei realizam treinamento médico no IPTAN</a>
                    <span>08/07/2016</span>
                </li>
                <li>
                    <a href="nota1.php">Alunos e professores do curso de Enfermagem do IPTAN atuam na equipe de primeiros socorros do III Jogos Mazarello</a>
                    <span>07/07/2016</span>
                </li>
                <!--<li>
                    <a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna...</a>
                    <span>02/05/2016</span>
                </li>
                <li>
                    <a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna...</a>
                    <span>02/05/2016</span>
                </li>
                <li>
                    <a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna...</a>
                    <span>02/05/2016</span>
                </li>
                <li>
                    <a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna...</a>
                    <span>02/05/2016</span>
                </li>-->
                
                
            </ul>
        </div>
    </div>
<!-- NOTÍCIAS -->
    
    
    
    
</div>
    

    
    <? require 'footer.php'; ?> 
    
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.hoverdir.js" type="text/javascript"></script>
    
    <script src="js/responsive-gride.js" type="text/javascript"></script>
    <script type="text/javascript">
    $('.grid').responsivegrid({
        'breakpoints': {
            'desktop' : {
                'range' : '*',
                'options' : {
                    'column' : 4,
                    'gutter' : '10px',
                    'itemHeight' : '100%',
                }
            },
            'tablet-landscape' : {
                'range' : '1000-1200',
                'options' : {
                    'column' : 3,
                    'gutter' : '10px',
                    'itemHeight' : '100%',
                }
            },
            'tablet-portrate' : {
                'range' : '767-1000',
                'options' : {
                    'column' : 2,
                    'gutter' : '5px',
                    'itemHeight' : '100%',
                }
            },
            'mobile' : {
                'range' : '-767',
                'options' : {
                    'column' : 2,
                    'gutter' : '5px',
                    'itemHeight' : '100%',
                }
            },
        }
    });
</script>
    
    <script>
        $("#btnMenu").click(function(){
        $("#btnMenu").toggleClass("active");
    });
        
    $("#listDates li .clickCalendar").click(function(){
        $("#listDates li .clickCalendar").removeClass("active");
        $(this).addClass("active");
        $(".tootipDescription").removeClass("active");
        $(this).next(".tootipDescription").addClass("active");
    });
    $(".tootipDescription").click(function(){
        $(".clickCalendar").removeClass("active");
        $(".tootipDescription").removeClass("active");
    });
    $(".clickCalendar, .tootipDescription").click(function(){
        var viewActive = $(".clickCalendar").hasClass("active");
        if(viewActive == true){
            $(".clickCalendar").css("opacity", "0.5");
        }else{
            $(".clickCalendar").css("opacity", "1");
        }
    });
        
    $("#openCloseMap").click(function(){
        $("#mapSite").toggleClass("openMapsFooter");
        
        var openMap = $("#mapSite").hasClass("openMapsFooter");
        if(openMap == true){
            $("#openCloseMap .fa-times").css("display", "inline-block");
            $("#openCloseMap .fa-expand").css("display", "none");
        }else{
            $("#openCloseMap .fa-times").css("display", "none");
            $("#openCloseMap .fa-expand").css("display", "inline-block");
        }
    });
        
        $(function() {
			
				$(' .listDetails > li ').each( function() { $(this).hoverdir(); } );

			});
    $("#menuHome > li").hover(function(){
        if ($(this).hasClass('active')) {
            
        } else {
            $("#menuHome > li").removeClass("active");
        }
    });
     $("#btnServices").click(function(){
        $(".serviceOnline").toggleClass("active");
    });
    $("#menuHome > li").click(function(){
        $("#menuHome > li").removeClass("active");
        $(this).addClass("active");
    });
        
        $("#btnMenu").click(function(){
        $("#menuHome").toggleClass("active");
        $("body").toggleClass("bodyFix");
    });
        
    
    var widthSite = $(window).width();
    if(widthSite >= 1050){
        $(window).scroll(function (event) {
                var rolado = $(window).scrollTop();
                if (rolado > 100) {
                    $("#barTop").addClass("reduce");
                } else {
                    $("#barTop").removeClass("reduce");
                }
            });
        }
        
        
    //SLIDE
    $('.carousel').carousel();
    </script>

    
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.6";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

</body>
</html>
