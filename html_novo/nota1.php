<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>IPTAN</title>
	<meta name="Author" content=""/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon-16x16.png">
    
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/html-reset.css">
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
    <link rel="stylesheet" type="text/css" href="css/blog.css">
     <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/base.css">
    <link rel="stylesheet" type="text/css" href="css/vendor.min.css">
    <link href='https://fonts.googleapis.com/css?family=Pontano+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <script src="js/modernizr.custom.js"></script>
</head>
<body>

<? require 'header.php'; ?>      
    
<div id="content">
    <ul id="breadcrumb">
        <li>
            <a href="index.php">Home</a>
        </li>
        <li>
            <a href="noticias.php">Notícias</a>
        </li>
        <li>
            <a href="nota1.php">Blog</a>
        </li>
    </ul>
    
    <div class="centerContent">
        
        <div class="barLeft">
             <div class="btnShare">
                <i class="fa fa-share-alt" aria-hidden="true"></i>
            </div>
            <div id="redes">
                <div class="fb-share-button" 
                    data-href="http://www.your-domain.com/your-page.php" 
                    data-layout="button_count">
                </div>
                <div class="g-plus" data-action="share" data-annotation="bubble"></div>
            </div>
            <h1>Alunos e professores do curso de Enfermagem do IPTAN atuam na equipe de primeiros socorros do III Jogos Mazarello</h1>
            <p align="justify">
                Alunos e professores do curso de Enfermagem do IPTAN a convite do Instituto Auxiliadora, participam dos III Jogos Mazzarello Regionais 2016. Os alunos foram convidados a participar da equipe de primeiros socorros do torneio. Cerca de 13 pessoas do curso de enfermagem estão se revezando em equipes durante os jogos que acontecem entre os dias 06 e 09 de julho no Instituto Auxiliadora. O evento promove a pratica de esportes entre alunos do ensino fundamental e ensino médio de Belo Horizonte, Ponte Nova, São João del-Rei e Barbacena. Com essa ação mais uma vez o IPTAN reforça a vivência profissional dos alunos de Enfermagem que tem a oportunidade de colocar em prática a abordagem correta sobre primeiros socorros e o primeiro atendimento, saindo da sala de aula e exercendo a pratica da profissão. Para a coordenadora do curso de enfermagem, Daniela Soares, esta é uma parceria que insere o IPTAN na comunidade trazendo segurança aos participantes dos jogos, realizando desta forma um aprendizado profissional para os alunos do curso de enfermagem do IPTAN.
            </p>
            
            <div class="listLinks">
            <div id="portfolio-wrapper" class="bgrid-fourth s-bgrid-third tab-bgrid-half">


            <div class="bgrid folio-item">
               <div class="item-wrap">
                  <a href="#modal-01">
                      <img src="images/portfolio/underwater.jpg" alt="Underwater" class="height:400px;">
                     <div class="overlay"></div>                       
                     <div class="portfolio-item-meta">
                              <h5></h5>
                        <p></p>
                           </div> 
                     <div class="link-icon"><i class="fa fa-plus"></i></div>
                  </a>
               </div>
            </div> <!-- item end -->

            <div class="bgrid folio-item">
               <div class="item-wrap">
                  <a href="#modal-02">
                     <img src="images/portfolio/hotel.jpg" alt="Hotel Sign">
                     <div class="overlay">
                      <div class="portfolio-item-meta">
                               <h5> </h5>
                           <p></p>
                            </div>
                     </div>
                     <div class="link-icon"><i class="fa fa-plus"></i></div>
                  </a>
               </div>
            </div> <!-- item end -->

            <div class="bgrid folio-item">
               <div class="item-wrap">
                  <a href="#modal-03">
                     <img src="images/portfolio/beetle.jpg" alt="Beetle">                        
                     <div class="overlay">
                      <div class="portfolio-item-meta">
                               <h5></h5>
                           <p></p>
                            </div>
                     </div>
                     <div class="link-icon"><i class="fa fa-plus"></i></div>
                  </a>
               </div>
            </div> <!-- item end -->

            <div class="bgrid folio-item">
               <div class="item-wrap">
                  <a href="#modal-04">
                     <img src="images/portfolio/banjo-player.jpg" alt="Banjo Player">
                     <div class="overlay">
                      <div class="portfolio-item-meta">
                               <h5></h5>
                           <p></p>
                            </div>
                     </div>
                     <div class="link-icon"><i class="fa fa-plus"></i></div>
                  </a>
               </div>
            </div> <!-- item end -->

            <div class="bgrid folio-item">
               <div class="item-wrap">
                  <a href="#modal-05">
                   <img src="images/portfolio/lighthouse.jpg" alt="Lighthouse">
                     <div class="overlay">
                      <div class="portfolio-item-meta">
                               <h5></h5>
                           <p></p>
                            </div>
                     </div>
                     <div class="link-icon"><i class="fa fa-plus"></i></div>
                  </a>
               </div>
            </div> <!-- item end -->

            <div class="bgrid folio-item">
               <div class="item-wrap">
                  <a href="#modal-06">
                     <img src="images/portfolio/girl.jpg" alt="Girl Stuff">
                     <div class="overlay">
                      <div class="portfolio-item-meta">
                               <h5></h5>
                           <p></p>
                            </div>
                     </div>
                     <div class="link-icon"><i class="fa fa-plus"></i></div>
                  </a>
               </div>
            </div> <!-- item end -->

            <div class="bgrid folio-item">
               <div class="item-wrap">
                  <a href="#modal-07">                        
                     <img src="images/portfolio/coffee.jpg" alt="Coffee Cup">                       
                     <div class="overlay">
                      <div class="portfolio-item-meta">
                               <h5></h5>
                           <p></p>
                            </div>
                     </div>
                     <div class="link-icon"><i class="fa fa-plus"></i></div>
                  </a>
               </div>
            </div> <!-- item end -->

            <div class="bgrid folio-item">
               <div class="item-wrap">
                  <a href="#modal-08">
                     <img src="images/portfolio/judah.jpg" alt="Judah">
                     <div class="overlay">
                        <div class="portfolio-item-meta">
                                  <h5></h5>
                           <p></p>
                               </div>
                     </div>
                     <div class="link-icon"><i class="fa fa-plus"></i></div>                     
                  </a>
               </div>
            </div>  <!-- item end -->

         </div> <!-- end portfolio-wrapper -->

         

    </div>
            
            <div class="tagsSearch">
            <h3>Tags</h3>
            <span>CURSO</span> <span>IPTAN</span> <span>JOGOS MAZARELLO</span> <span>EXTENSÃO</span> <span>ENFERMAGEM</span>
            </div>
        </div>
        <div class="barRight">
            <h5>
                <i class="fa fa-clock-o" aria-hidden="true"></i>
                <span>Recentes</span>
            </h5>
            <ul>
                <li>
                    <a href="nota3.php">
                        <span>14/07/2016</span>
                       <p>Time de Futsal do Iptan disputa pela primeira vez o (JUMs 2016) Jogos Universitários Mineiros.</p> 
                    </a>
                </li>
                <li>
                    <a href="nota2.php">
                        <span>08/07/2016</span>
                       <p>Militares 11<font face="Arial, Helvetica, sans-serif">º</font> Batalhão de Infantaria de São João del-Rei realizam treinamento médico no IPTAN</p> 
                    </a>
                </li>
                <li>
                    <a href="nota1.php">
                        <span>07/07/2016</span>
                       <p>Alunos e professores do curso de Enfermagem do IPTAN atuam na equipe de primeiros socorros do III Jogos Mazarello</p> 
                    </a>
                </li>
            </ul>
        </div>
        
    </div>
    
    
    
    
</div>
    
    
<? require 'footer.php'; ?> 
    
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.hoverdir.js" type="text/javascript"></script>
    
    
    <script>
        
        $("#btnMenu").click(function(){
        $("#btnMenu").toggleClass("active");
    });
        
    $("#openCloseMap").click(function(){
        $("#mapSite").toggleClass("openMapsFooter");
        
        var openMap = $("#mapSite").hasClass("openMapsFooter");
        if(openMap == true){
            $("#openCloseMap .fa-times").css("display", "inline-block");
            $("#openCloseMap .fa-expand").css("display", "none");
        }else{
            $("#openCloseMap .fa-times").css("display", "none");
            $("#openCloseMap .fa-expand").css("display", "inline-block");
        }
    });
        
        $(function() {
			
				$(' .listDetails > li ').each( function() { $(this).hoverdir(); } );

			});
    $("#menuHome > li").hover(function(){
        if ($(this).hasClass('active')) {
            
        } else {
            $("#menuHome > li").removeClass("active");
        }
    });
     $("#btnServices").click(function(){
        $(".serviceOnline").toggleClass("active");
    });
    $("#menuHome > li").click(function(){
        $("#menuHome > li").removeClass("active");
        $(this).addClass("active");
    });
        
        $("#btnMenu").click(function(){
        $("#menuHome").toggleClass("active");
        $("body").toggleClass("bodyFix");
    });
        
        $(".btnShare").click(function(){
        $("#redes").toggleClass("active");
    });
        
    
    var widthSite = $(window).width();
    if(widthSite >= 1050){
        $(window).scroll(function (event) {
                var rolado = $(window).scrollTop();
                if (rolado > 100) {
                    $("#barTop").addClass("reduce");
                } else {
                    $("#barTop").removeClass("reduce");
                }
            });
        }
    </script>

    
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.6";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    
    
<script src="https://apis.google.com/js/platform.js" async defer>
        {lang: 'pt-BR'}
    </script>
    
    
<script src="js/jquery.waypoints.min.js"></script>
   <script src="js/jquery.magnific-popup.min.js"></script>  
<script>
(function($) {
    $('.item-wrap a').magnificPopup({
       type:'inline',
       fixedContentPos: false,
       removalDelay: 300,
       showCloseBtn: false,
       mainClass: 'mfp-fade'

    });
    $(document).on('click', '.popup-modal-dismiss', function (e) {
            e.preventDefault();
            $.magnificPopup.close();
    });
})(jQuery);
</script>

</body>
</html>
