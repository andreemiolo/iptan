<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>IPTAN</title>
	<meta name="Author" content=""/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon-16x16.png">
    
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/html-reset.css">
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
    <link rel="stylesheet" type="text/css" href="css/coringa.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/base.css">
    <link rel="stylesheet" type="text/css" href="css/vendor.min.css">
    <link href='https://fonts.googleapis.com/css?family=Pontano+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <script src="js/modernizr.custom.js"></script>
</head>
<body>
    
<!-- <a href="#" class="btnCourse">
    Quero saber mais sobre este curso
</a>
-->

<? require 'header.php'; ?>      
    
    
<div id="content">
    <ul id="breadcrumb">
        <li>
            <a href="index.php">Home</a>
        </li>
        <li>
            <a href="#">Graduação</a>
        </li>
        <li>
            <a href="cursos.php">Cursos</a>
        </li>
        <li>
            <a href="direito.php">Direito</a>
        </li> 
    </ul>
    
    <div class="standardTitle dir">
        <div class="centerContent">
            <div id="redes">
                <div class="fb-share-button" 
                    data-href="http://www.your-domain.com/your-page.php" 
                    data-layout="button_count">
                </div>
                <div class="g-plus" data-action="share" data-annotation="bubble"></div>
            </div>
            <h1 class="titleDefault">
                <span>Graduação</span>
                <p>Direito</p><br />
            </h1>
            <div class="text">
                <p align="justify">O curso de Direito forma profissionais com sólida base jurídica, capazes de atuar de forma consciente em face das demandas transculturais decorrentes do processo de globalização, permitindo o adequado desenvolvimento pelo formando das habilidades e competências recomendadas pelas diretrizes curriculares nacionais. Para tanto, o quadro docente do curso é composto por operadores do Direito e ciências afins, todos dotados de vasto conhecimento teórico e de prática profissional. Quanto ao perfil profissional, o bacharel em Direito desenvolve visão crítica acerca dos fenômenos jurídicos e está apto a assessorar empresas e órgãos públicos e constituir escritório próprio de advocacia como profissional liberal. Pode concorrer a cargos públicos, como promotor, juiz de direito, delegado de polícia, procurador federal, estadual ou municipal, defensor público e carreiras diplomáticas, entre outros. No mercado de trabalho, o profissional de Direito pode atuar, ainda, como profissional autônomo, nos serviços públicos, na assessoria a órgãos públicos e em empresas privadas, instituições financeiras e escritórios de advocacia.
                </p>
            </div>
            
            <ul class="topicsTitle dircolor">
                <li>
                    <span class="centericon fa fa-graduation-cap fa-2x"></span><h5 class="textbox">Graduação</h5>
                    <p class="textbox">Bacharelado</p>
                </li>
                <li>
                    <span class="centericon fa fa-dollar fa-2x"></span><h5 class="textbox">Valor do Curso</h5>
                    <p class="textbox">R$ 686,00</p>
                </li>
                <li>
                    <span class="centericon fa fa-flag fa-2x"></span><h5 class="textbox">Área de Conhecimento</h5>
                    <p class="textbox">Ciências Sociais Aplicadas</p>
                </li>
                <li>
                    <span class="centericon fa fa-calendar-o fa-2x"></span><h5 class="textbox">Duração do Curso</h5>
                    <p class="textbox">10 semestres</p>
                </li>
            </ul>
        </div>
    </div>
    
    <div class="centerContent">
        
        <div class="barLeft">
            <h1 class="titleDefault2">
                <span>Coordenação</span>
                <p style="font-size:20px;">Antônio Américo de Campos Júnior</p>
            </h1>
            <p align="justify">
                Possui graduação em Direito pela Universidade Federal de Minas Gerais (1985), especialização em Direito Público Municipal pela Escola Superior do Ministério Público de Minas Gerais (2001) e mestrado em Direito pela Universidade Gama Filho (2005). Advogado licenciado da Prefeitura do Município de Barbacena (MG), Procurador-Geral do Município de São João del-Rei (MG), Coordenador do Curso de Direito e professor adjunto do Instituto de Ensino Superior Presidente Tancredo de Almeida Neves (IPTAN) e professor adjunto licenciado da Universidade Presidente Antônio Carlos (UNIPAC), em Barbacena (MG). Tem experiência na área de Direito, com ênfase em Direito Público.
            </p>
            <!--<div class="tagsSearch">
                <h3>Tags</h3>
                <span>CURSO</span> <span>IPTAN</span> <span>PALESTRA</span> <span>EXTENSÃO</span> <span>VESTIBULAR</span>
            </div>-->
        </div>
        <div class="barRight">
            <div class="blockRight">
            <h5>
                <i class="fa fa-link" aria-hidden="true"></i>
                <span>Matriz Curricular</span>
            </h5>
            <ul>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/direito/matriz-curricular.pdf" target="_blank">Matriz</a>
                </li>
            </ul>
            </div>
            <div class="blockRight">
            <h5>
                <i class="fa fa-arrow-down" aria-hidden="true"></i>
                <span>Arquivos</span>
            </h5>
            <ul>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/horarios_de_aula/horario_aula_dir_d.pdf" target="_blank">Horário de aula Diurno</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/horarios_de_aula/horario_aula_dir_n.pdf" target="_blank">Horário de aula Noturno</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/direito/projeto-pedagogico.pdf" target="_blank">Projeto Pedagógico</a>
                </li>
            </ul>
            </div>
            <div class="blockRight">
            <h5>
                <i class="fa fa-link" aria-hidden="true"></i>
                <span>Regulamentos</span>
            </h5>
                <ul>
                    <li>
                        <a href="http://www.iptan.edu.br/arquivos-cursos/direito/regulamento-monografia.pdf" target="_blank">Monografia</a>
                    </li>
                    <li>
                        <a href="http://www.iptan.edu.br/arquivos-cursos/direito/regulamento-npj-estagios.pdf" target="_blank">NPJ e Estágios</a>
                    </li>
                </ul>
            </div>
            <div class="blockRight">
            <h5>
                <i class="fa fa-link" aria-hidden="true"></i>
                <span>Plano de Ensino</span>
            </h5>
            <ul>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/direito/plano-de-ensino/1-periodo.rar" target="_blank">1<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/direito/plano-de-ensino/2-periodo.rar" target="_blank">2<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/direito/plano-de-ensino/3-periodo.rar" target="_blank">3<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/direito/plano-de-ensino/4-periodo.rar" target="_blank">4<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/direito/plano-de-ensino/5-periodo.rar" target="_blank">5<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/direito/plano-de-ensino/6-periodo.rar" target="_blank">6<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/direito/plano-de-ensino/8-periodo.rar" target="_blank">7<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/direito/plano-de-ensino/8-periodo.rar" target="_blank">8<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/direito/plano-de-ensino/9-periodo.rar" target="_blank">9<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/direito/plano-de-ensino/10-periodo.rar" target="_blank">10<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
            </ul>
            </div>
        </div>
        
    </div>
           
    
</div>
    
    
<? require 'footer.php'; ?> 
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.hoverdir.js" type="text/javascript"></script>
    
    
    <script>
        
        $("#btnMenu").click(function(){
        $("#btnMenu").toggleClass("active");
    });
        
    $("#openCloseMap").click(function(){
        $("#mapSite").toggleClass("openMapsFooter");
        
        var openMap = $("#mapSite").hasClass("openMapsFooter");
        if(openMap == true){
            $("#openCloseMap .fa-times").css("display", "inline-block");
            $("#openCloseMap .fa-expand").css("display", "none");
        }else{
            $("#openCloseMap .fa-times").css("display", "none");
            $("#openCloseMap .fa-expand").css("display", "inline-block");
        }
    });
        
        $(function() {
			
				$(' .listDetails > li ').each( function() { $(this).hoverdir(); } );

			});
    $("#menuHome > li").hover(function(){
        if ($(this).hasClass('active')) {
            
        } else {
            $("#menuHome > li").removeClass("active");
        }
    });
     $("#btnServices").click(function(){
        $(".serviceOnline").toggleClass("active");
    });
    $("#menuHome > li").click(function(){
        $("#menuHome > li").removeClass("active");
        $(this).addClass("active");
    });
        
        $("#btnMenu").click(function(){
        $("#menuHome").toggleClass("active");
        $("body").toggleClass("bodyFix");
    });
        
    
    var widthSite = $(window).width();
    if(widthSite >= 1050){
        $(window).scroll(function (event) {
                var rolado = $(window).scrollTop();
                if (rolado > 100) {
                    $("#barTop").addClass("reduce");
                } else {
                    $("#barTop").removeClass("reduce");
                }
            });
        }
    </script>

    
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.6";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    
    
<script src="https://apis.google.com/js/platform.js" async defer>
        {lang: 'pt-BR'}
    </script>
   <script src="js/jquery.waypoints.min.js"></script>
   <script src="js/jquery.magnific-popup.min.js"></script>  
<script>
(function($) {
    $('.item-wrap a').magnificPopup({
       type:'inline',
       fixedContentPos: false,
       removalDelay: 300,
       showCloseBtn: false,
       mainClass: 'mfp-fade'

    });
    $(document).on('click', '.popup-modal-dismiss', function (e) {
    		e.preventDefault();
    		$.magnificPopup.close();
    });
})(jQuery);
</script>

</body>
</html>
