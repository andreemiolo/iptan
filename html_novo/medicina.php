<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>IPTAN</title>
	<meta name="Author" content=""/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon-16x16.png">
    
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/html-reset.css">
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
    <link rel="stylesheet" type="text/css" href="css/coringa.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/base.css">
    <link rel="stylesheet" type="text/css" href="css/vendor.min.css">
    <link href='https://fonts.googleapis.com/css?family=Pontano+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <script src="js/modernizr.custom.js"></script>
</head>
<body>
    
<!-- <a href="#" class="btnCourse">
    Quero saber mais sobre este curso
</a>
-->

<? require 'header.php'; ?>      
    
    
<div id="content">
    <ul id="breadcrumb">
        <li>
            <a href="index.php">Home</a>
        </li>
        <li>
            <a href="#">Graduação</a>
        </li>
        <li>
            <a href="cursos.php">Cursos</a>
        </li>
        <li>
            <a href="medicina.php">Medicina</a>
        </li>
    </ul>
    
    <div class="standardTitle med">
        <div class="centerContent">
            <div id="redes">
                <div class="fb-share-button" 
                    data-href="http://www.your-domain.com/your-page.php" 
                    data-layout="button_count">
                </div>
                <div class="g-plus" data-action="share" data-annotation="bubble"></div>
            </div>
            <h1 class="titleDefault">
                <span>Graduação</span>
                <p>Medicina</p><br />
            </h1>
            <div class="text">
                <p align="justify">O curso de MEDICINA do Instituto de Ensino Superior Presidente Tancredo de Almeida Neves (IPTAN) foi criado em 2015, autorizado pela Portaria MEC nº 502 de 02/07/2015, publicada no Diário Oficial da União em 03/07/2015, reconhecido com conceito 4 (MEC).<br />
                São oferecidas, anualmente, 38 vagas. A graduação é realizada em 6 (seis) anos, em sistema de módulos anuais. Nosso diferencial é priorizar aspectos relacionados à atitude profissional médica e formar profissionais com participação responsável no sistema de saúde. Para isso, optamos por uma metodologia ativa e integrada. Assim, disciplinas que tradicionalmente são oferecidas em módulos separados, encontram-se integradas, intencionalmente combinadas. Além disso, para incentivar e instigar uma aprendizagem efetiva, realizamos aulas subdividindo a turma em pequenos grupos de, no máximo, 10 pessoas.<br />
                A instituição conta com todos os materiais e laboratórios necessários para uma formação profissional de excelência. Somando-se a isso, as salas de aula são equipadas com lousas digitais (e-boards) e há uma sala de aula estruturada no modelo interativo de aprendizagem. Toda a formação é acompanhada por especialistas, seja dentro da sala de aula, com professores de alta qualidade e ampla experiência na área; seja fora da sala de aula, através do Núcleo de Apoio Psicopedagógico aos estudantes de Medicina (NAPEM).<br />
                O curso de Medicina do IPTAN visa a preparar os futuros médicos não apenas para o conhecimento dos conceitos e das técnicas médicas, mas também para o desenvolvimento de habilidades e atitudes que humanizam o exercício da profissão.
                </p>
            </div>
            
            <ul class="topicsTitle medcolor">
                <li>
                    <span class="centericon fa fa-graduation-cap fa-2x"></span><h5 class="textbox">Graduação</h5>
                    <p class="textbox">Bacharelado</p>
                </li>
                <li>
                    <span class="centericon fa fa-dollar fa-2x"></span><h5 class="textbox">Valor do Curso</h5>
                    <p class="textbox">R$ 8.580,00</p>
                </li>
                <li>
                    <span class="centericon fa fa-flag fa-2x"></span><h5 class="textbox">Área de Conhecimento</h5>
                    <p class="textbox">Ciências Médicas e da Saúde</p>
                </li>
                <li>
                    <span class="centericon fa fa-calendar-o fa-2x"></span><h5 class="textbox">Duração do Curso</h5>
                    <p class="textbox">12 semestres</p>
                </li>
            </ul>
        </div>
    </div>
    
    <div class="centerContent">
        
        <div class="barLeft">
            <h1 class="titleDefault2">
                <span>Coordenação</span>
                <p style="font-size:20px;">Carlos André Dilascio Detomi</p>
            </h1>
            <p align="justify">
                Possui graduação em Medicina pela Universidade Federal de Juiz de Fora(1991), especialização em Trauma na Infância e Adolescência pelo Fundação Educacional Lucas Machado(2005), residencia-medicapela HOSPITAL SANTA RITA(1993) e residencia-medicapela Hospital da Baleia(1996). Atualmente é Médico do corpo clínico da HOSPITAL NOSSA SENHORA DAS MERCÊS, Médico do corpo clínico da SANTA CASA DE MISERICÓRDIA DE SÃO JOÃO DEL REI, Médico intervencionista da SAMU CISRU CENTRO SUL, Médico pediatra da PREFEITURA MUNICIPAL DE CONCEIÇÃO DA BARRA DE MINAS, Médico cirurgião pediatra do CONSÓRCIO INTERMUNICIPAL DE SAÚDE DAS VERTENTES, coordenador do curso de medicina do Instituto de Ensino Superior Presidente Tancredo de Almeida Neves e professor do curso de medicina do Instituto de Ensino Superior Presidente Tancredo de Almeida Neves. Tem experiência na área de Medicina, com ênfase em Cirurgia.
            </p>
            <!--<div class="tagsSearch">
                <h3>Tags</h3>
                <span>CURSO</span> <span>IPTAN</span> <span>PALESTRA</span> <span>EXTENSÃO</span> <span>VESTIBULAR</span>
            </div>-->
        </div>
        <div class="barRight">
            <h5>
                <i class="fa fa-link" aria-hidden="true"></i>
                <span>Matriz Curricular</span>
            </h5>
            <ul>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/medicina/matriz-curricular.pdf" target="_blank">Matriz</a>
                </li>
            </ul>
            <h5>
                <i class="fa fa-arrow-down" aria-hidden="true"></i>
                <span>Arquivos</span>
            </h5>
            <ul>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/horarios_de_aula/horario_aula_med_1_1p.pdf" target="_blank">Horário de aula 1<font face="Arial, Helvetica, sans-serif">º</font> ano - Período 1</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/horarios_de_aula/horario_aula_med_1_2p.pdf" target="_blank">Horário de aula 1<font face="Arial, Helvetica, sans-serif">º</font> ano - Período 2</a>
                </li>
            </ul>
            <h5>
                <i class="fa fa-link" aria-hidden="true"></i>
                <span>Regulamentos</span>
            </h5>
                <ul>
                    <li>
                        <a href="http://www.iptan.edu.br/arquivos-cursos/medicina/regulamentos_medicina_2016.pdf" target="_blank">Atividades Complementares</a>
                    </li>
                </ul>
        </div>
        
    </div>
    <!--
    <div id="portfolio-wrapper" class="centerContent bgrid-fourth s-bgrid-third tab-bgrid-half">


            <div class="bgrid folio-item">
               <div class="item-wrap">
                  <a href="#modal-01">
                      <img src="images/portfolio/underwater.jpg" alt="Underwater" class="height:400px;">
                     <div class="overlay"></div>                       
                     <div class="portfolio-item-meta">
                              <h5></h5>
                        <p></p>
                           </div> 
                     <div class="link-icon"><i class="fa fa-plus"></i></div>
                  </a>
               </div>
            </div> 

            <div class="bgrid folio-item">
               <div class="item-wrap">
                  <a href="#modal-02">
                     <img src="images/portfolio/hotel.jpg" alt="Hotel Sign">
                     <div class="overlay">
                      <div class="portfolio-item-meta">
                               <h5></h5>
                           <p></p>
                            </div>
                     </div>
                     <div class="link-icon"><i class="fa fa-plus"></i></div>
                  </a>
               </div>
            </div> 

            <div class="bgrid folio-item">
               <div class="item-wrap">
                  <a href="#modal-03">
                     <img src="images/portfolio/beetle.jpg" alt="Beetle">                        
                     <div class="overlay">
                      <div class="portfolio-item-meta">
                               <h5></h5>
                           <p></p>
                            </div>
                     </div>
                     <div class="link-icon"><i class="fa fa-plus"></i></div>
                  </a>
               </div>
            </div> 

            <div class="bgrid folio-item">
               <div class="item-wrap">
                  <a href="#modal-04">
                     <img src="images/portfolio/banjo-player.jpg" alt="Banjo Player">
                     <div class="overlay">
                      <div class="portfolio-item-meta">
                               <h5></h5>
                           <p></p>
                            </div>
                     </div>
                     <div class="link-icon"><i class="fa fa-plus"></i></div>
                  </a>
               </div>
            </div> 

            <div class="bgrid folio-item">
               <div class="item-wrap">
                  <a href="#modal-05">
                   <img src="images/portfolio/lighthouse.jpg" alt="Lighthouse">
                     <div class="overlay">
                      <div class="portfolio-item-meta">
                               <h5></h5>
                           <p></p>
                            </div>
                     </div>
                     <div class="link-icon"><i class="fa fa-plus"></i></div>
                  </a>
               </div>
            </div> 

            <div class="bgrid folio-item">
               <div class="item-wrap">
                  <a href="#modal-06">
                     <img src="images/portfolio/girl.jpg" alt="Girl Stuff">
                     <div class="overlay">
                      <div class="portfolio-item-meta">
                               <h5></h5>
                           <p></p>
                            </div>
                     </div>
                     <div class="link-icon"><i class="fa fa-plus"></i></div>
                  </a>
               </div>
            </div> 

            <div class="bgrid folio-item">
               <div class="item-wrap">
                  <a href="#modal-07">                        
                     <img src="images/portfolio/coffee.jpg" alt="Coffee Cup">                       
                     <div class="overlay">
                      <div class="portfolio-item-meta">
                               <h5></h5>
                           <p></p>
                            </div>
                     </div>
                     <div class="link-icon"><i class="fa fa-plus"></i></div>
                  </a>
               </div>
            </div> 

            <div class="bgrid folio-item">
               <div class="item-wrap">
                  <a href="#modal-08">
                     <img src="images/portfolio/judah.jpg" alt="Judah">
                     <div class="overlay">
                        <div class="portfolio-item-meta">
                                  <h5></h5>
                           <p></p>
                               </div>
                     </div>
                     <div class="link-icon"><i class="fa fa-plus"></i></div>                     
                  </a>
               </div>
            </div>  

         </div> <!-- end portfolio-wrapper -->
       -->
    
</div>
    
    <? require 'footer.php'; ?> 
    
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.hoverdir.js" type="text/javascript"></script>
    
    
    <script>
        
        $("#btnMenu").click(function(){
        $("#btnMenu").toggleClass("active");
    });
        
    $("#openCloseMap").click(function(){
        $("#mapSite").toggleClass("openMapsFooter");
        
        var openMap = $("#mapSite").hasClass("openMapsFooter");
        if(openMap == true){
            $("#openCloseMap .fa-times").css("display", "inline-block");
            $("#openCloseMap .fa-expand").css("display", "none");
        }else{
            $("#openCloseMap .fa-times").css("display", "none");
            $("#openCloseMap .fa-expand").css("display", "inline-block");
        }
    });
        
        $(function() {
			
				$(' .listDetails > li ').each( function() { $(this).hoverdir(); } );

			});
    $("#menuHome > li").hover(function(){
        if ($(this).hasClass('active')) {
            
        } else {
            $("#menuHome > li").removeClass("active");
        }
    });
     $("#btnServices").click(function(){
        $(".serviceOnline").toggleClass("active");
    });
    $("#menuHome > li").click(function(){
        $("#menuHome > li").removeClass("active");
        $(this).addClass("active");
    });
        
        $("#btnMenu").click(function(){
        $("#menuHome").toggleClass("active");
        $("body").toggleClass("bodyFix");
    });
        
    
    var widthSite = $(window).width();
    if(widthSite >= 1050){
        $(window).scroll(function (event) {
                var rolado = $(window).scrollTop();
                if (rolado > 100) {
                    $("#barTop").addClass("reduce");
                } else {
                    $("#barTop").removeClass("reduce");
                }
            });
        }
    </script>

    
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.6";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    
    
<script src="https://apis.google.com/js/platform.js" async defer>
        {lang: 'pt-BR'}
    </script>
   <script src="js/jquery.waypoints.min.js"></script>
   <script src="js/jquery.magnific-popup.min.js"></script>  
<script>
(function($) {
    $('.item-wrap a').magnificPopup({
       type:'inline',
       fixedContentPos: false,
       removalDelay: 300,
       showCloseBtn: false,
       mainClass: 'mfp-fade'

    });
    $(document).on('click', '.popup-modal-dismiss', function (e) {
    		e.preventDefault();
    		$.magnificPopup.close();
    });
})(jQuery);
</script>

</body>
</html>
