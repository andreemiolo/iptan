<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>IPTAN</title>
	<meta name="Author" content=""/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon-16x16.png">
    
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/html-reset.css">
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
    <link rel="stylesheet" type="text/css" href="css/coringa.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/base.css">
    <link rel="stylesheet" type="text/css" href="css/vendor.min.css">
    <link href='https://fonts.googleapis.com/css?family=Pontano+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <script src="js/modernizr.custom.js"></script>
</head>
<body>
    
<!-- <a href="#" class="btnCourse">
    Quero saber mais sobre este curso
</a>
-->

<? require 'header.php'; ?>      
    
    
<div id="content">
    <ul id="breadcrumb">
        <li>
            <a href="index.php">Home</a>
        </li>
        <li>
            <a href="#">Graduação</a>
        </li>
        <li>
            <a href="cursos.php">Cursos</a>
        </li>
        <li>
            <a href="cienciascontabeis.php">Ciências Contábeis</a>
        </li>        
    </ul>
    
    <div class="standardTitle cic">
        <div class="centerContent">
            <div id="redes">
                <div class="fb-share-button" 
                    data-href="http://www.your-domain.com/your-page.php" 
                    data-layout="button_count">
                </div>
                <div class="g-plus" data-action="share" data-annotation="bubble"></div>
            </div>
            <h1 class="titleDefault">
                <span>Graduação</span>
                <p>Ciências Contábeis</p><br />
            </h1>
            <div class="text">
                <p align="justify">O Curso de Ciências Contábeis definiu seu perfil inicialmente a partir dos objetivos de informar e formar o futuro profissional, de modo que ele pudesse desenvolver habilidades para operar e obter o máximo de resultados econômico-financeiros para a gestão empresarial, acessar informações e, sobretudo, saber buscar meios para responder dúvidas e solucionar problemas relativos às áreas contábil, financeira, de custos e gerencial.<br />
                O profissional poderá atuar em uma gama de atividades dentro da área contábil, conforme exposto por Marion (2010), desempenhando atividades e/ou serviços afins e correlatos em indústrias, empresas em geral, entidades assistenciais, implantando o seu próprio escritório contábil, consultoria contábil, ou ainda nas seguintes atividades: Supervisão e orientação técnica; Estudo, planejamento, projeto e especificação; Estudo de viabilidade técnico-econômica; Assistência, assessoria e consultoria; Serviço técnico de auditoria; Vistoria, perícia, avaliação, arbitramento, laudo e parecer técnico; Desempenho de cargo e função técnica no setor privado ou público; Elaboração de orçamento; Padronização, mensuração e controle de qualidade de informações econômicas, contábeis e financeiras; Produção técnica especializada; Condução de trabalho técnico e acadêmico; Docência técnica e superior.
                </p>
            </div>
            
            <ul class="topicsTitle ciccolor">
                <li>
                    <span class="centericon fa fa-graduation-cap fa-2x"></span><h5 class="textbox">Graduação</h5>
                    <p class="textbox">Bacharelado</p>
                </li>
                <li>
                    <span class="centericon fa fa-dollar fa-2x"></span><h5 class="textbox">Valor do Curso</h5>
                    <p class="textbox">R$ 487,00</p>
                </li>
                <li>
                    <span class="centericon fa fa-flag fa-2x"></span><h5 class="textbox">Área de Conhecimento</h5>
                    <p class="textbox">Ciências Sociais Aplicadas</p>
                </li>
                <li>
                    <span class="centericon fa fa-calendar-o fa-2x"></span><h5 class="textbox">Duração do Curso</h5>
                    <p class="textbox">8 semestres</p>
                </li>
            </ul>
        </div>
    </div>
    
    <div class="centerContent">
        
        <div class="barLeft">
            <h1 class="titleDefault2">
                <span>Coordenação</span>
                <p style="font-size:20px;">Carla Agostini</p>
            </h1>
            <p align="justify">
                Atualmente é coordenadora e professora do curso de Ciências Contábeis do Instituto de Ensino Superior Presidente Tancredo de Almeida Neves (IPTAN). Membro do Núcleo Docente Estruturante - NDE e Colegiado. Orientadora de Trabalho de Conclusão de Curso - TCC, no curso de Administração Pública da UFSJ, na modalidade à Distância. Mestre em Administração pela FEAD - MG (2013). Possui MBA em Gestão de Pessoas pela UFSJ, graduação em Ciências Contábeis pela Universidade Presidente Antônio Carlos (2005).
            </p>
            <!--<div class="tagsSearch">
                <h3>Tags</h3>
                <span>CURSO</span> <span>IPTAN</span> <span>PALESTRA</span> <span>EXTENSÃO</span> <span>VESTIBULAR</span>
            </div>-->
        </div>
        <div class="barRight">
            <h5>
                <i class="fa fa-link" aria-hidden="true"></i>
                <span>Matriz Curricular</span>
            </h5>
            <ul>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/contabeis/matriz-curricular.pdf" target="_blank">Matriz</a>
                </li>
            </ul>
            <h5>
                <i class="fa fa-arrow-down" aria-hidden="true"></i>
                <span>Arquivos</span>
            </h5>
            <ul>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/horarios_de_aula/horario_aula_cc.pdf" target="_blank">Horário de aula</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/contabeis/projeto-pedagogico.pdf" target="_blank">Projeto Pedagógico</a>
                </li>
            </ul>
            <h5>
                <i class="fa fa-link" aria-hidden="true"></i>
                <span>Regulamentos</span>
            </h5>
                <ul>
                    <li>
                        <a href="http://www.iptan.edu.br/arquivos-cursos/contabeis/regulamento-tcc.pdf" target="_blank">TCC - Trabalho de Conclusão de Curso</a>
                    </li>
                    <li>
                        <a href="http://www.iptan.edu.br/arquivos-cursos/contabeis/regulamento-atividades-complementares.pdf" target="_blank">Atividades Complementares</a>
                    </li>
                </ul>
            <h5>
                <i class="fa fa-link" aria-hidden="true"></i>
                <span>Plano de Ensino</span>
            </h5>
            <ul>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/contabeis/plano-de-ensino/1-periodo.rar" target="_blank">1<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/contabeis/plano-de-ensino/2-periodo.rar" target="_blank">2<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/contabeis/plano-de-ensino/3-periodo.rar" target="_blank">3<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/contabeis/plano-de-ensino/4-periodo.rar" target="_blank">4<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/contabeis/plano-de-ensino/5-periodo.rar" target="_blank">5<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/contabeis/plano-de-ensino/6-periodo.rar" target="_blank">6<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/contabeis/plano-de-ensino/8-periodo.rar" target="_blank">7<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/contabeis/plano-de-ensino/8-periodo.rar" target="_blank">8<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
            </ul>
        </div>
        
    </div>
           
    
</div>
    
    
<? require 'footer.php'; ?> 
    
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.hoverdir.js" type="text/javascript"></script>
    
    
    <script>
        
        $("#btnMenu").click(function(){
        $("#btnMenu").toggleClass("active");
    });
        
    $("#openCloseMap").click(function(){
        $("#mapSite").toggleClass("openMapsFooter");
        
        var openMap = $("#mapSite").hasClass("openMapsFooter");
        if(openMap == true){
            $("#openCloseMap .fa-times").css("display", "inline-block");
            $("#openCloseMap .fa-expand").css("display", "none");
        }else{
            $("#openCloseMap .fa-times").css("display", "none");
            $("#openCloseMap .fa-expand").css("display", "inline-block");
        }
    });
        
        $(function() {
			
				$(' .listDetails > li ').each( function() { $(this).hoverdir(); } );

			});
    $("#menuHome > li").hover(function(){
        if ($(this).hasClass('active')) {
            
        } else {
            $("#menuHome > li").removeClass("active");
        }
    });
     $("#btnServices").click(function(){
        $(".serviceOnline").toggleClass("active");
    });
    $("#menuHome > li").click(function(){
        $("#menuHome > li").removeClass("active");
        $(this).addClass("active");
    });
        
        $("#btnMenu").click(function(){
        $("#menuHome").toggleClass("active");
        $("body").toggleClass("bodyFix");
    });
        
    
    var widthSite = $(window).width();
    if(widthSite >= 1050){
        $(window).scroll(function (event) {
                var rolado = $(window).scrollTop();
                if (rolado > 100) {
                    $("#barTop").addClass("reduce");
                } else {
                    $("#barTop").removeClass("reduce");
                }
            });
        }
    </script>

    
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.6";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    
    
<script src="https://apis.google.com/js/platform.js" async defer>
        {lang: 'pt-BR'}
    </script>
   <script src="js/jquery.waypoints.min.js"></script>
   <script src="js/jquery.magnific-popup.min.js"></script>  
<script>
(function($) {
    $('.item-wrap a').magnificPopup({
       type:'inline',
       fixedContentPos: false,
       removalDelay: 300,
       showCloseBtn: false,
       mainClass: 'mfp-fade'

    });
    $(document).on('click', '.popup-modal-dismiss', function (e) {
    		e.preventDefault();
    		$.magnificPopup.close();
    });
})(jQuery);
</script>

</body>
</html>
