<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>IPTAN</title>
	<meta name="Author" content=""/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon-16x16.png">
    
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/html-reset.css">
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
    <link rel="stylesheet" type="text/css" href="css/coringa.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/base.css">
    <link rel="stylesheet" type="text/css" href="css/vendor.min.css">
    <link href='https://fonts.googleapis.com/css?family=Pontano+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <script src="js/modernizr.custom.js"></script>
</head>
<body>
    
<!-- <a href="#" class="btnCourse">
    Quero saber mais sobre este curso
</a>
-->

<? require 'header.php'; ?>      
    
    
<div id="content">
    <ul id="breadcrumb">
        <li>
            <a href="index.php">Home</a>
        </li>
        <li>
            <a href="#">Graduação</a>
        </li>
        <li>
            <a href="cursos.php">Cursos</a>
        </li>
        <li>
            <a href="odontologia.php">Odontologia</a>
        </li>
    </ul>
    
    <div class="standardTitle odt">
        <div class="centerContent">
            <div id="redes">
                <div class="fb-share-button" 
                    data-href="http://www.your-domain.com/your-page.php" 
                    data-layout="button_count">
                </div>
                <div class="g-plus" data-action="share" data-annotation="bubble"></div>
            </div>
            <h1 class="titleDefault">
                <span>Graduação</span>
                <p>Odontologia</p><br />
            </h1>
            <div class="text">
                <p align="justify">O curso de Odontologia do IPTAN tem o seu início no ano de 2016, sendo oferecidas 80 vagas por semestre, sendo 40 no período da manhã e 40 no período da noite. O curso de Odontologia tem a duração de 5 anos, divididos em 10 períodos.
O curso tem como objetivo a formação de um profissional da área da saúde capaz de compreender seu paciente como um ser biopsicossocial, em que a percepção de uma sociedade ativa está diretamente relacionada com a saúde bucal, biológica e mental.
Para que isso seja possível, lançamos mão de metodologias ativas e integradas, fazendo com que nossos alunos possam relacionar as diferentes áreas da Odontologia e da Saúde em geral durante sua formação acadêmica.
Em nossa estrutura física contamos com salas de aula equipadas com lousas digitais (e-boards) e salas estruturadas no modelo interativo de aprendizagem, além de professores altamente capacitados para estimular nossos alunos em busca da excelência.
A Odontologia do IPTAN se empenha para formar cirurgiões-dentistas que apresentem um conhecimento técnico-científico atualizado e de alto nível, sempre fiéis ao preceito da honestidade e comprometidos com a sociedade de forma humanizada.</p>
            </div>
            
            <ul class="topicsTitle odtcolor">
                <li>
                    <span class="centericon fa fa-graduation-cap fa-2x"></span><h5 class="textbox">Graduação</h5>
                    <p class="textbox">Bacharelado</p>
                </li>
                <li>
                    <span class="centericon fa fa-dollar fa-2x"></span><h5 class="textbox">Valor do Curso</h5>
                    <p class="textbox">R$ 1.650,00</p>
                </li>
                <li>
                    <span class="centericon fa fa-flag fa-2x"></span><h5 class="textbox">Área de Conhecimento</h5>
                    <p class="textbox">Ciências Médicas e da Saúde</p>
                </li>
                <li>
                    <span class="centericon fa fa-calendar-o fa-2x"></span><h5 class="textbox">Duração do Curso</h5>
                    <p class="textbox">10 semestres</p>
                </li>
            </ul>
        </div>
    </div>
    
    <div class="centerContent">
        
        <div class="barLeft">
           <h1 class="titleDefault2">
                <span>Coordenação</span>
                <p style="font-size:20px;">Marcel Abrão</p>
            </h1>
            <p align="justify">
                Mestre em Odontologia com ênfase em Ortodontia pela Pontifícia Universidade Católica de Minas Gerais. Especialista em Ortodontia pela Pontifícia Universidade Católica de Minas Gerais. Curso de Aperfeiçoamento em Ortodontia e Ortopedia Facial pela Integrale. Graduado em Odontologia pela Universidade Estadual de Londrina.
            </p>
            <!--
            <div class="tagsSearch">
                <h3>Tags</h3>
                <span>CURSO</span> <span>IPTAN</span> <span>PALESTRA</span> <span>EXTENSÃO</span> <span>VESTIBULAR</span>
            </div>
            -->
        </div>
        <!--
        <div class="barRight">
            <h5>
                <i class="fa fa-link" aria-hidden="true"></i>
                <span>Acesso</span>
            </h5>
            <ul>
                <li>
                    <a href="#">Hoc vero, quod nimirum</a>
                </li>
                <li>
                    <a href="#">Hoc vero, quo</a>
                </li>
                <li>
                    <a href="#">A quod nimirum</a>
                </li>
                <li>
                    <a href="#">Hoc vemirum</a>
                </li>
            </ul>
            <h5>
                <i class="fa fa-arrow-down" aria-hidden="true"></i>
                <span>
                Download
                    </span>
            </h5>
            <ul>
                <li>
                    <a href="#">Hoc vero, quod nimirum</a>
                </li>
                <li>
                    <a href="#">Hoc vero, quo</a>
                </li>
                <li>
                    <a href="#">A quod nimirum</a>
                </li>
                <li>
                    <a href="#">Hoc vemirum</a>
                </li>
            </ul>
        </div>
        
    </div>-->
            
    
</div>
    
<? require 'footer.php'; ?> 
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.hoverdir.js" type="text/javascript"></script>
    
    
    <script>
        
        $("#btnMenu").click(function(){
        $("#btnMenu").toggleClass("active");
    });
        
    $("#openCloseMap").click(function(){
        $("#mapSite").toggleClass("openMapsFooter");
        
        var openMap = $("#mapSite").hasClass("openMapsFooter");
        if(openMap == true){
            $("#openCloseMap .fa-times").css("display", "inline-block");
            $("#openCloseMap .fa-expand").css("display", "none");
        }else{
            $("#openCloseMap .fa-times").css("display", "none");
            $("#openCloseMap .fa-expand").css("display", "inline-block");
        }
    });
        
        $(function() {
			
				$(' .listDetails > li ').each( function() { $(this).hoverdir(); } );

			});
    $("#menuHome > li").hover(function(){
        if ($(this).hasClass('active')) {
            
        } else {
            $("#menuHome > li").removeClass("active");
        }
    });
     $("#btnServices").click(function(){
        $(".serviceOnline").toggleClass("active");
    });
    $("#menuHome > li").click(function(){
        $("#menuHome > li").removeClass("active");
        $(this).addClass("active");
    });
        
        $("#btnMenu").click(function(){
        $("#menuHome").toggleClass("active");
        $("body").toggleClass("bodyFix");
    });
        
    
    var widthSite = $(window).width();
    if(widthSite >= 1050){
        $(window).scroll(function (event) {
                var rolado = $(window).scrollTop();
                if (rolado > 100) {
                    $("#barTop").addClass("reduce");
                } else {
                    $("#barTop").removeClass("reduce");
                }
            });
        }
    </script>

    
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.6";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    
    
<script src="https://apis.google.com/js/platform.js" async defer>
        {lang: 'pt-BR'}
    </script>
   <script src="js/jquery.waypoints.min.js"></script>
   <script src="js/jquery.magnific-popup.min.js"></script>  
<script>
(function($) {
    $('.item-wrap a').magnificPopup({
       type:'inline',
       fixedContentPos: false,
       removalDelay: 300,
       showCloseBtn: false,
       mainClass: 'mfp-fade'

    });
    $(document).on('click', '.popup-modal-dismiss', function (e) {
    		e.preventDefault();
    		$.magnificPopup.close();
    });
})(jQuery);
</script>

</body>
</html>
