<div class="footer">
    <div class="centerContent" id="topFooter">
        <div id="logoInfo">
            <img src="images/assinatura.png">
        </div>
        <div id="logoInfo">
            <p>© 2016 | Todos os direitos reservados</p>
            <p>Avenida Leite de Castro, 1101, Fábricas - São João del-Rei - MG - 36301-182</p>
            <p>(32) 3379-2725</p>
        </div>
        <div id="logoInfo">
            <div id="redes">
                <a href="https://www.facebook.com/iptansjdr/?fref=ts" target="_blank">
                    <i class="fa fa-facebook-official" aria-hidden="true"></i>
                </a>
                <a href="#">
                    <i class="fa fa-youtube" aria-hidden="true"></i>
                </a>
            </div>
        </div>
        <!--div id="facebook">
            <div class="fb-page" data-href="https://www.facebook.com/iptansjdr/?fref=ts" data-tabs="timeline" data-width="600" data-height="200" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/iptansjdr/?fref=ts"><a href="https://www.facebook.com/iptansjdr/?fref=ts">IPTAN</a></blockquote></div></div>
        </div -->
    </div>
    
    <div id="mapSite">
        <div class="centerContent">
            <div id="openCloseMap">
                <i class="fa fa-times" aria-hidden="true"></i>
                <i class="fa fa-expand" aria-hidden="true"></i>
                <p>mapa de site</p>
            </div>
        <ul>
            <li>
                <h4>Aluno</h4>
                <a href="http://186.194.210.79/corpore.net/Login.aspx" target="_blank">Portal Educacional</a>
                <!-- <a href="#">Avaliação Institucional</a> -->
                <a href="http://186.194.210.79/corpore.net/Source/Bib-Biblioteca/Public/BibFirewall.htm?CodColigada=1&CodFilial=1&CodUnidade=1" target="_blank">Biblioteca</a>
                <!-- <a href="#">Pesquisa</a> -->
                <!-- <a href="#">Extenção</a> -->
                <a href="http://www.iptan.soshost.com.br/solicitacao_ouvidoria.php" target="_blank">Ouvidoria</a>
                <a href="http://www.iptan.edu.br/arquivos-gerais/calendario-academico.pdf" target="_blank">Calendário Acadêmico</a>
            </li>
            <li>
                <h4>Professores e Funcionários</h4>
                <a href="http://186.194.210.79/corpore.net/Login.aspx" target="_blank">Portal Educacional</a>                
                <a href="https://webmail-seguro.com.br/iptan.edu.br/" target="_blank">Webmail</a>
                <a href="http://www.iptan.edu.br/sisti/index.php" target="_blank">SISTI</a>
                <a href="http://www.iptan.edu.br/reserva_equip/index.php" target="_blank">SIS Reserva</a>
                <a href="http://www.iptan.soshost.com.br/solicitacao_ouvidoria.php" target="_blank">Ouvidoria</a>
                <a href="http://186.194.210.79/corpore.net/Source/Bib-Biblioteca/Public/BibFirewall.htm?CodColigada=1&CodFilial=1&CodUnidade=1" target="_blank">Biblioteca</a>
            </li>
        </ul>
        <ul>
            <li>
                <h4>Institucional</h4>
                <a href="iptan.php">O IPTAN</a>
                <a href="laboratorios.php">Laboratórios</a>
                <a href="avaliacaoinstitucional.php">Avaliação Institucional</a>                
                <!-- <a href="#">Corpo Diretivo</a> -->
                <h4>Administrativo</h4>
                <a href="biblioteca.php">Biblioteca</a>
                <a href="extensaouniversitaria.php">Coordenação de Extenção</a>
                <a href="coordenacaodepesquisa.php">Coordenação de Pesquisa</a>
                <a href="financeiro.php">Financeiro</a>
                <a href="nucleodeapoioaoestudante.php">Núcleo de Apoio ao Estudante</a>
                <a href="secretariaacademica.php">Secretária Acadêmica</a>
                <a href="tecnologiadainformacao.php">Tecnologia da Informação</a>
            </li>
            <li>
                <h4>Graduação</h4>
                <a href="http://www.iptan.edu.br/novovestibular/" target="_blank">Vestibular</a>
                <a href="cursos.php">Cursos</a>
                <!-- <a href="#">Matrícula</a> -->
                <!-- <a href="#">Rematrícula</a> -->
                <!-- <a href="#">Faça IPTAN</a> -->
                <a href="http://www.iptan.edu.br/arquivos-gerais/calendario-academico.pdf" target="_blank">Calendário Acadêmico</a>
                <a href="http://www.iptan.edu.br/creditar/" target="_blank">Financiamento Privado</a>
            </li>
            <li>
                <h4><a href="http://www.itpac.br/sites/educacao-a-distancia" target="_blank" class="linkTitle">EAD</a></h4>
                <!-- <a href="#">Polo EAD IPTAN</a> -->
            </li>
            <li>
                <h4><a href="pesquisaiptan.php" class="linkTitle">Pesquisa</a></h4>
                <!--<a href="#">Pesquisa em Andamento</a>
                <a href="#">Iniciação Científica</a>
                <a href="#">Publicações</a> -->
            </li>
            <li>
                <h4><a href="extensaoiptan.php" class="linkTitle">Extensão</a></h4>
                <!-- <a href="#">Cursos</a>
                <a href="#">Projetos IPTAN</a> -->
            </li>
            <li>
                <h4><a href="http://www.iptan.edu.br/novovestibular/" target="_blank" class="linkTitle">Vestibular</a></h4>
                <!-- <a href="#">Vestibular de Medicina</a>
                <a href="#">Vestibular Geral</a> -->
            </li>
            <li>

                <h4><a href="noticias.php" class="linkTitle"> Notícias</a></h4>
            </li>
            <li>
                <h4>Contato</h4>
                <a href="contato.php">Fale com IPTAN</a>
                <!-- <a href="#">Ramais</a> -->
            </li>
        </ul>
        </div>
    </div>
</div>