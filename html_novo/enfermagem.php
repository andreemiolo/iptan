<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>IPTAN</title>
	<meta name="Author" content=""/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon-16x16.png">
    
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/html-reset.css">
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
    <link rel="stylesheet" type="text/css" href="css/coringa.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/base.css">
    <link rel="stylesheet" type="text/css" href="css/vendor.min.css">
    <link href='https://fonts.googleapis.com/css?family=Pontano+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <script src="js/modernizr.custom.js"></script>
</head>
<body>
    
<!-- <a href="#" class="btnCourse">
    Quero saber mais sobre este curso
</a>
-->
<? require 'header.php'; ?>      
    
<div id="content">
    <ul id="breadcrumb">
        <li>
            <a href="index.php">Home</a>
        </li>
        <li>
            <a href="#">Graduação</a>
        </li>
        <li>
            <a href="cursos.php">Cursos</a>
        </li>
        <li>
            <a href="enfermagem.php">Enfermagem</a>
        </li>
    </ul>
    
    <div class="standardTitle enf">
        <div class="centerContent">
            <div id="redes">
                <div class="fb-share-button" 
                    data-href="http://www.your-domain.com/your-page.php" 
                    data-layout="button_count">
                </div>
                <div class="g-plus" data-action="share" data-annotation="bubble"></div>
            </div>
            <h1 class="titleDefault">
                <span>Graduação</span>
                <p>Enfermagem</p><br />
            </h1>
            <div class="text">
                <p align="justify">É um curso da área da saúde que permite ao profissional atuar nas áreas de ensino, pesquisa e em diferentes níveis de atenção à saúde, no âmbito da promoção, prevenção e recuperação da saúde, incluindo hospitais, clínicas, ambulatórios, indústrias, atenção domiciliar e demais serviços assistenciais públicos e privados. O curso de Enfermagem do IPTAN está estruturado para formar profissionais com pensamento critico e reflexivo, com postura ética, liderança e responsabilidade social. Tem duração de cinco anos e é ministrado majoritariamente em horário noturno, sendo os estágios em horário diurno. O objetivo do curso é fornecer ao aluno uma completa capacitação técnica e profissional, direcionada ao desenvolvimento do cuidado e à atuação multidisciplinar com segurança e competência, contribuindo para a qualidade da atenção à saúde e humanização no atendimento prestado a indivíduos, famílias e comunidades.
                </p>
            </div>
            
            <ul class="topicsTitle enfcolor">
                <li>
                    <span class="centericon fa fa-graduation-cap fa-2x"></span><h5 class="textbox">Graduação</h5>
                    <p class="textbox">Bacharelado</p>
                </li>
                <li>
                    <span class="centericon fa fa-dollar fa-2x"></span><h5 class="textbox">Valor do Curso</h5>
                    <p class="textbox">R$ 754,00</p>
                </li>
                <li>
                    <span class="centericon fa fa-flag fa-2x"></span><h5 class="textbox">Área de Conhecimento</h5>
                    <p class="textbox">Ciências Médicas e da Saúde</p>
                </li>
                <li>
                    <span class="centericon fa fa-calendar-o fa-2x"></span><h5 class="textbox">Duração do Curso</h5>
                    <p class="textbox">8 semestres</p>
                </li>
            </ul>
        </div>
    </div>
    
    <div class="centerContent">
        
        <div class="barLeft">
            <h1 class="titleDefault2">
                <span>Coordenação</span>
                <p style="font-size:20px;">Daniela Soares Santos</p>
            </h1>
            <p align="justify">
				Doutora em Enfermagem pela Escola de Enfermagem da UFMG, Membro do Núcleo de Pesquisa sobre Administração em Enfermagem (NUPAE), Docente e Coordenadora do curso de graduação em enfermagem do Instituto de Ensino Superior Presidente Tancredo de Almeida Neves (IPTAN). Revisora Ad Hoc para Revista Brasileira de Pesquisa em Saúde e Revista Docência do Ensino Superior. Mestre e Graduada em Enfermagem pela Universidade Federal de Minas Gerais. Possuo experiência nas áreas de Ensino, Pesquisa (linha de pesquisa: Organização e gestão de serviços de saúde e de enfermagem) e Gestão de Pessoas, com atuação nas seguintes funções/cargos: Coordenação de estágios curriculares em hospital de ensino, atuando na negociação de estágios curriculares e formalização de convênios com Instituições de Ensino de Belo Horizonte para cursos de graduação, no planejamento, organização, acompanhamento e avaliação dos estágios desenvolvidos no Hospital Odilon Behrens (HOB); participação da comissão de elaboração dos projetos Residência Multiprofissional em Saúde (2009 e 2010) aprovados pelo MEC/MS; coordenação do Escritório de Projetos da Gerência de Ensino e Pesquisa do HOB, atuando na captação de editais de financiamento (2008-2011). Docente da Faculdade da Saúde e Ecologia Humana - FASEH e Coordenação do Projeto de Extensão &quot;Posso Ajudar?&quot; (2009-2012). Assessora de Desenvolvimento de Projetos da Pró-Reitoria de Recursos Humanos da UFMG (03/2012), com atuação na elaboração e análise do Dimensionamento da Força de Trabalho da UFMG e de projetos de capacitação de servidores TAE.
            </p>
            <!--<div class="tagsSearch">
                <h3>Tags</h3>
                <span>CURSO</span> <span>IPTAN</span> <span>PALESTRA</span> <span>EXTENSÃO</span> <span>VESTIBULAR</span>
            </div>-->
        </div>
        <div class="barRight">
            <h5>
                <i class="fa fa-link" aria-hidden="true"></i>
                <span>Matriz Curricular</span>
            </h5>
            <ul>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/enfermagem/matriz-curricular.pdf" target="_blank">Matriz</a>
                </li>
            </ul>
            <h5>
                <i class="fa fa-arrow-down" aria-hidden="true"></i>
                <span>Arquivos</span>
            </h5>
            <ul>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/horarios_de_aula/horario_aula_enf.pdf" target="_blank">Horário de aula</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/enfermagem/projeto-pedagogico.pdf" target="_blank">Projeto Pedagógico</a>
                </li>
            </ul>
            <h5>
                <i class="fa fa-link" aria-hidden="true"></i>
                <span>Regulamentos</span>
            </h5>
                <ul>
                    <li>
                        <a href="http://www.iptan.edu.br/arquivos-cursos/enfermagem/regulamento-tcc.pdf" target="_blank">TCC - Trabalho de Conclusão de Curso</a>
                    </li>
                    <li>
                        <a href="http://www.iptan.edu.br/arquivos-cursos/enfermagem/regulamento-atividades-complementares.pdf" target="_blank">Atividades Complementares</a>
                    </li>
                    <li>
                        <a href="http://www.iptan.edu.br/arquivos-cursos/enfermagem/regulamento-estagio-supervisionado.pdf" target="_blank">Estágio Supervisionado</a>
                    </li>
                    <li>
                        <a href="http://www.iptan.edu.br/arquivos-cursos/enfermagem/regulamento-laboratorios.pdf" target="_blank">Utilização de Laboratórios</a>
                    </li>
                </ul>
            <h5>
                <i class="fa fa-link" aria-hidden="true"></i>
                <span>Plano de Ensino</span>
            </h5>
            <ul>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/enfermagem/plano-de-ensino/1-periodo.rar" target="_blank">1<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/enfermagem/plano-de-ensino/2-periodo.rar" target="_blank">2<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/enfermagem/plano-de-ensino/3-periodo.rar" target="_blank">3<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/enfermagem/plano-de-ensino/4-periodo.rar" target="_blank">4<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/enfermagem/plano-de-ensino/5-periodo.rar" target="_blank">5<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/enfermagem/plano-de-ensino/6-periodo.rar" target="_blank">6<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/enfermagem/plano-de-ensino/8-periodo.rar" target="_blank">7<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/enfermagem/plano-de-ensino/8-periodo.rar" target="_blank">8<font face="Arial, Helvetica, sans-serif">º</font> Período</a>
                </li>
                <li>
                    <a href="http://www.iptan.edu.br/arquivos-cursos/enfermagem/plano-de-ensino/9-10-periodo.rar" target="_blank">9<font face="Arial, Helvetica, sans-serif">º</font> e 10<font face="Arial, Helvetica, sans-serif">º</font> Períodos</a>
                </li>
            </ul>
        </div>
        
    </div>     
    
</div>
    
<? require 'footer.php'; ?> 
    
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.hoverdir.js" type="text/javascript"></script>
    
    
    <script>
        
        $("#btnMenu").click(function(){
        $("#btnMenu").toggleClass("active");
    });
        
    $("#openCloseMap").click(function(){
        $("#mapSite").toggleClass("openMapsFooter");
        
        var openMap = $("#mapSite").hasClass("openMapsFooter");
        if(openMap == true){
            $("#openCloseMap .fa-times").css("display", "inline-block");
            $("#openCloseMap .fa-expand").css("display", "none");
        }else{
            $("#openCloseMap .fa-times").css("display", "none");
            $("#openCloseMap .fa-expand").css("display", "inline-block");
        }
    });
        
        $(function() {
			
				$(' .listDetails > li ').each( function() { $(this).hoverdir(); } );

			});
    $("#menuHome > li").hover(function(){
        if ($(this).hasClass('active')) {
            
        } else {
            $("#menuHome > li").removeClass("active");
        }
    });
     $("#btnServices").click(function(){
        $(".serviceOnline").toggleClass("active");
    });
    $("#menuHome > li").click(function(){
        $("#menuHome > li").removeClass("active");
        $(this).addClass("active");
    });
        
        $("#btnMenu").click(function(){
        $("#menuHome").toggleClass("active");
        $("body").toggleClass("bodyFix");
    });
        
    
    var widthSite = $(window).width();
    if(widthSite >= 1050){
        $(window).scroll(function (event) {
                var rolado = $(window).scrollTop();
                if (rolado > 100) {
                    $("#barTop").addClass("reduce");
                } else {
                    $("#barTop").removeClass("reduce");
                }
            });
        }
    </script>

    
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.6";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    
    
<script src="https://apis.google.com/js/platform.js" async defer>
        {lang: 'pt-BR'}
    </script>
   <script src="js/jquery.waypoints.min.js"></script>
   <script src="js/jquery.magnific-popup.min.js"></script>  
<script>
(function($) {
    $('.item-wrap a').magnificPopup({
       type:'inline',
       fixedContentPos: false,
       removalDelay: 300,
       showCloseBtn: false,
       mainClass: 'mfp-fade'

    });
    $(document).on('click', '.popup-modal-dismiss', function (e) {
    		e.preventDefault();
    		$.magnificPopup.close();
    });
})(jQuery);
</script>

</body>
</html>
