<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>IPTAN</title>
	<meta name="Author" content=""/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon-16x16.png">
    
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/html-reset.css">
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
    <link rel="stylesheet" type="text/css" href="css/contato.css">
    <link href='https://fonts.googleapis.com/css?family=Pontano+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <script src="js/modernizr.custom.js"></script>
</head>
<body>
    
<? require 'header.php'; ?>      
    
    
<div id="content">

    
        <div class="centerContent">
            <h1>Fale com o IPTAN</h1>
            
            <form id="formContact" action="contato_envia.php" method="post" name="form1">
                <div class="row">
                  <div class="col-xs-4">
                    <label for="exampleInputEmail1">Nome*</label>
                    <input type="text" name="nome" class="form-control" id="exampleInputEmail1" placeholder="Nome" required>
                  </div>
                <div class="col-xs-3">
                    <label for="exampleInputEmail1">Email*</label>
                    <input type="text" name="email" class="form-control" id="exampleInputEmail1" placeholder="E-mail" required>
                  </div>
                <div class="col-xs-3">
                    <label for="exampleInputEmail1">Telefone</label>
                    <input type="text" name="telefone" class="form-control" id="exampleInputEmail1" placeholder="Telefone">
                  </div>
                </div>
                <!--
                <div class="row">
                <div class="col-xs-4">
                    <label for="exampleInputEmail1">Setor*</label>
                    <select class="form-control">
                      <option>1</option>
                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                    </select>
                </div>
                <div class="col-xs-4">
                    <label for="exampleInputEmail1">Assunto*</label>
                    <select class="form-control">
                      <option>1</option>
                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                    </select>
                </div>
                </div>
                -->
                    
                <div class="row">
                    <div class="col-xs-10">
                        <label for="exampleInputEmail1">Mensagem*</label>
                    <textarea class="form-control" name="mensagem" rows="6" placeholder="Mensagem" required></textarea>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-xs-10">
                        <button type="submit" class="btn btn-default">
                            <span>
                            Enviar
                                </span>
                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
            </form>
            
        </div>
    
    
    <div id="overlay" class="map">
    <iframe id="map" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3721.785756013202!2d-44.246716!3d-21.121106000000005!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x794e946f2342947!2sInstituto+de+Ensino+Superior+Presidente+Tancredo+de+Almeida+Neves!5e0!3m2!1spt-BR!2sus!4v1463513294878" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
  
</div>
    

    
<? require 'footer.php'; ?> 
    
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.hoverdir.js" type="text/javascript"></script>
    
    <script>
        
        $("#btnMenu").click(function(){
        $("#btnMenu").toggleClass("active");
    });
        
    $("#listDates li .clickCalendar").click(function(){
        $("#listDates li .clickCalendar").removeClass("active");
        $(this).addClass("active");
        $(".tootipDescription").removeClass("active");
        $(this).next(".tootipDescription").addClass("active");
    });
    $(".tootipDescription").click(function(){
        $(".clickCalendar").removeClass("active");
        $(".tootipDescription").removeClass("active");
    });
    $(".clickCalendar, .tootipDescription").click(function(){
        var viewActive = $(".clickCalendar").hasClass("active");
        if(viewActive == true){
            $(".clickCalendar").css("opacity", "0.5");
        }else{
            $(".clickCalendar").css("opacity", "1");
        }
    });
        
    $("#openCloseMap").click(function(){
        $("#mapSite").toggleClass("openMapsFooter");
        
        var openMap = $("#mapSite").hasClass("openMapsFooter");
        if(openMap == true){
            $("#openCloseMap .fa-times").css("display", "inline-block");
            $("#openCloseMap .fa-expand").css("display", "none");
        }else{
            $("#openCloseMap .fa-times").css("display", "none");
            $("#openCloseMap .fa-expand").css("display", "inline-block");
        }
    });
        
        $(function() {
			
				$(' .listDetails > li ').each( function() { $(this).hoverdir(); } );

			});
    $("#menuHome > li").hover(function(){
        if ($(this).hasClass('active')) {
            
        } else {
            $("#menuHome > li").removeClass("active");
        }
    });
     $("#btnServices").click(function(){
        $(".serviceOnline").toggleClass("active");
    });
    $("#menuHome > li").click(function(){
        $("#menuHome > li").removeClass("active");
        $(this).addClass("active");
    });
        
        $("#btnMenu").click(function(){
        $("#menuHome").toggleClass("active");
        $("body").toggleClass("bodyFix");
    });
        
    
    var widthSite = $(window).width();
    if(widthSite >= 1050){
        $(window).scroll(function (event) {
                var rolado = $(window).scrollTop();
                if (rolado > 100) {
                    $("#barTop").addClass("reduce");
                } else {
                    $("#barTop").removeClass("reduce");
                }
            });
        }
        
        
    //SLIDE
    $('.carousel').carousel();
        
    
    </script>
    
    <script>
    $(document).ready(function () {
        $('#map').addClass('scrolloff');                // set the mouse events to none when doc is ready
        
        $('#overlay').on("mouseup",function(){          // lock it when mouse up
            $('#map').addClass('scrolloff'); 
            //somehow the mouseup event doesn't get call...
        });
        $('#overlay').on("mousedown",function(){        // when mouse down, set the mouse events free
            $('#map').removeClass('scrolloff');
        });
        $("#map").mouseleave(function () {              // becuase the mouse up doesn't work... 
            $('#map').addClass('scrolloff');            // set the pointer events to none when mouse leaves the map area
                                                        // or you can do it on some other event
        });
        
    });
</script>

    
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.6";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

</body>
</html>
